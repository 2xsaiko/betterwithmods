/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;

import javax.annotation.Nullable;

@SuppressWarnings("deprecation")
public abstract class BlockDigitalRedstone extends BlockBase {

    public static final BooleanProperty LIT = BlockStateProperties.LIT;

    public BlockDigitalRedstone(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(LIT, false));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(LIT);
    }


    protected boolean isCurrentlyValid(World worldIn, BlockPos pos, IBlockState state) {
        boolean lit = isLit(worldIn, pos);
        boolean powered = worldIn.getRedstonePowerFromNeighbors(pos) > 0;
        return lit == powered;
    }

    protected boolean isLit(IBlockReader worldIn, BlockPos pos) {
        return worldIn.getBlockState(pos).get(LIT);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, @Nullable EntityLivingBase placer, ItemStack stack) {
        handleChanges(state, worldIn, pos);
    }

    public void handleChanges(IBlockState state, World worldIn, BlockPos pos) {
        if (!worldIn.isRemote) {
            if (!isCurrentlyValid(worldIn, pos, state)) {
                boolean lit = isLit(worldIn, pos);
                worldIn.setBlockState(pos, state.with(LIT, !lit), 2);
                if (isLit(worldIn, pos)) {
                    onPowered(state, worldIn, pos);
                } else {
                    onUnpowered(state, worldIn, pos);
                }
            }

        }
    }

    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        handleChanges(state,worldIn,pos);
        onChanged(state, worldIn, pos, blockIn, fromPos);
    }


    public int tickRate(IWorldReaderBase worldIn) {
        return 4;
    }

    public abstract void onPowered(IBlockState state, World worldIn, BlockPos pos);

    public abstract void onUnpowered(IBlockState state, World worldIn, BlockPos pos);

    protected void onChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
    }


}
