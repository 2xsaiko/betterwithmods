/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.block;

import com.google.common.collect.Maps;
import net.minecraft.block.material.Material;

import java.util.Map;

public class MaterialKey {

    private final Material material;
    private final String name;

    public MaterialKey(Material material, String name) {
        this.material = material;
        this.name = name;
        MATERIALS.put(material, this);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        assert o instanceof MaterialKey;
        return this.hashCode() == o.hashCode();
    }

    public boolean matches(Material material) {
        MaterialKey other = get(material);
        if(other != null)
            return this.equals(other);
        return false;
    }

    public final static Map<Material, MaterialKey> MATERIALS = Maps.newHashMap();

    static {
        new MaterialKey(Material.ROCK, "rock");
        new MaterialKey(Material.WOOD, "wood");
    }

    public static MaterialKey get(Material material) {
        return MATERIALS.getOrDefault(material, null);
    }

}
