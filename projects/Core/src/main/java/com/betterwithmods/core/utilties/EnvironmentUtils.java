/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.particles.IParticleData;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import java.util.Arrays;

public class EnvironmentUtils {

    public static final VectorBuilder RANDOM_PARTICLE = new VectorBuilder().offset(0.5).setGaussian(0.30);
    public static final VectorBuilder SPEED = new VectorBuilder();

    public static void forEachParticleRandom(IWorld world, BlockPos pos, IParticleData particle, int count, VectorBuilder position, VectorBuilder speed, double chance) {
        if(Rand.chance(world.getRandom(), chance)) {
            forEachParticle(world,pos, particle,count, position,speed);
        }
    }

    public static void forEachParticle(IWorld world, BlockPos pos, IParticleData particle, int count, VectorBuilder position, VectorBuilder speed) {
        IParticleData[] particles = new IParticleData[count];
        Arrays.fill(particles, particle);
        forEachParticles(world, Arrays.asList(particles), position, speed, pos);
    }

    public static void forEachParticles(IWorld world, Iterable<IParticleData> particles, VectorBuilder position, VectorBuilder speed, BlockPos pos) {
        for (IParticleData particle : particles) {
            Vec3d p = position.build(pos);
            Vec3d s = speed.build(Vec3d.ZERO);
            spawnParticle(world, particle, p, s);
        }
    }

    public static void spawnParticle(IWorld world, IParticleData particle, Vec3d p, Vec3d s) {
        world.spawnParticle(particle, p.x, p.y, p.z, s.x, s.y, s.z);
    }

    public static void playRandomSound(final World world, final BlockPos pos, final SoundEvent sound, final SoundCategory category, final float volume, final float pitch, final double chance) {
        Rand.onRandom(world.rand, chance, () -> playSound(world, pos, sound, category, volume, pitch));
    }


    public static void playSound(World world, BlockPos pos, SoundEvent sound, SoundCategory category, float volume, float pitch) {
        if(world.isRemote()) {
            world.playSound(pos.getX() + 0.5d, pos.getY() + 0.5d, pos.getZ() + 0.5d, sound, category, volume, pitch, false);
        } else {
            world.playSound(null, pos, sound, category, volume,pitch);
        }
    }

}
