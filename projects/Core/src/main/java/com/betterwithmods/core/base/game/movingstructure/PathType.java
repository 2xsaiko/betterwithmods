package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;

import java.util.function.Function;

public enum PathType {
    Line(LinePath::load),
    ;

    private final Function<NBTTagCompound, ? extends Path> constructor;

    PathType(Function<NBTTagCompound, ? extends Path> constructor) {
        this.constructor = constructor;
    }

    public static Path load(NBTTagCompound nbt) {
        PathType type = PathType.values()[nbt.getInt("type")]; // FIXME
        return type.constructor.apply(nbt);
    }

    public static NBTTagCompound save(Path c) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInt("type", c.type().ordinal());
        c.save(tag);
        return tag;
    }

}
