/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.Optional;

public class ItemUtils {


    public static Optional<Block> getBlock(Item item) {
        if (item instanceof ItemBlock) {
            return Optional.of(((ItemBlock) item).getBlock());
        }
        return Optional.empty();
    }


    public static Optional<Block> getBlock(ItemStack stack) {
        if (!stack.isEmpty()) {
            return getBlock(stack.getItem());
        }
        return Optional.empty();
    }

    public static Optional<Item> getItem(ItemStack stack) {
        if (!stack.isEmpty())
            return Optional.of(stack.getItem());
        return Optional.empty();
    }

    public static StackEjector EJECT_OFFSET = new StackEjector(new VectorBuilder().rand(0.5f).offset(0.25f), new VectorBuilder().setGaussian(0.05f));

    public static StackEjector EJECT_EXACT = new StackEjector(new VectorBuilder(), new VectorBuilder());

    public static void ejectStackOffset(World world, BlockPos pos, Iterable<ItemStack> stacks) {
        for(ItemStack stack: stacks)
            ejectStackOffset(world,pos, stack);
    }

    public static void ejectStackOffset(World world, BlockPos pos, ItemStack stack) {
        ejectStackOffset(world, new Vec3d(pos), stack);
    }

    public static void ejectStackOffset(World world, Vec3d vec, ItemStack stack) {
        if (stack.isEmpty())
            return;
        EJECT_EXACT.setStack(stack).ejectStack(world, vec, Vec3d.ZERO);
    }

    public static void ejectStackExact(World world, BlockPos pos, Iterable<ItemStack> stacks) {
        for(ItemStack stack: stacks)
            ejectStackExact(world,pos, stack);
    }

    public static void ejectStackExact(World world, BlockPos pos, ItemStack stack) {
        ejectStackExact(world, new Vec3d(pos), stack);
    }

    public static void ejectStackExact(World world, Vec3d vec, ItemStack stack) {
        if (stack.isEmpty())
            return;
        EJECT_OFFSET.setStack(stack).ejectStack(world, vec, Vec3d.ZERO);
    }


}
