/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties.inventory;

import net.minecraftforge.items.IItemHandler;

import java.util.Iterator;

public class ItemHandlerIterator implements Iterator<ItemSlot> {

    private int i;
    private IItemHandler handler;

    public ItemHandlerIterator(IItemHandler handler) {
        this.handler = handler;
    }

    @Override
    public boolean hasNext() {
        return i < handler.getSlots();
    }

    @Override
    public ItemSlot next() {
        ItemSlot slot = new ItemSlot(handler, handler.getStackInSlot(i), i);
        i++;
        return slot;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not specific enough to remove from an IItemHandler");
    }
}
