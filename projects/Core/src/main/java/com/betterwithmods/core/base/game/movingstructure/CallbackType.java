package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;

import java.util.function.Function;

public enum CallbackType {
    Block(BlockCallback::load),
    ;

    private final Function<NBTTagCompound, ? extends Callback> constructor;

    CallbackType(Function<NBTTagCompound, ? extends Callback> constructor) {
        this.constructor = constructor;
    }

    public static Callback load(NBTTagCompound nbt) {
        CallbackType type = CallbackType.values()[nbt.getInt("type")]; // FIXME
        return type.constructor.apply(nbt);
    }

    public static NBTTagCompound save(Callback c) {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInt("type", c.type().ordinal());
        c.save(tag);
        return tag;
    }

}
