/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core;

import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.Shape;
import net.minecraft.util.ResourceLocation;

public class Relationships {

    public static final Relation OAK_WOOD = relation(References.MODID_CORE, "oak_wood");

    public static final Shape PLANK = shape(References.MODID_CORE, "plank");
    public static final Shape LOG = shape(References.MODID_CORE, "log");
    public static final Shape BARK = shape(References.MODID_CORE, "bark");
    public static final Shape DUST = shape(References.MODID_CORE, "dust");

    public static Relation.Wrapper relation(String mod, String name) {
        return relation(new ResourceLocation(mod, name));
    }

    public static Relation.Wrapper relation(ResourceLocation name) {
        return new Relation.Wrapper(name);
    }

    public static Shape.Wrapper shape(String mod, String name) {
        return shape(new ResourceLocation(mod, name));
    }

    public static Shape.Wrapper shape(ResourceLocation name) {
        return new Shape.Wrapper(name);
    }


}
