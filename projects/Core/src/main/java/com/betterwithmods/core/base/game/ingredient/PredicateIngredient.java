/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.ingredient;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PredicateIngredient extends Ingredient {

    private ItemStack[] matchingStacks = new ItemStack[0];
    private boolean matchingStacksCached;


    private Predicate<ItemStack> predicate;

    public PredicateIngredient(Predicate<ItemStack> predicate) {
        super(Stream.of());
        this.predicate = predicate;
    }

    @Override
    public boolean test(@Nullable ItemStack stack) {
        return predicate.test(stack);
    }

    @Override
    public ItemStack[] getMatchingStacks() {
        if (!matchingStacksCached)
            cacheMatchingStacks();
        return matchingStacks;
    }

    public void cacheMatchingStacks() {
        ArrayList<ItemStack> matches = new ArrayList<>();
        for (Item item : ForgeRegistries.ITEMS) {
            ItemGroup group = item.getGroup();
            NonNullList<ItemStack> stacks = NonNullList.create();
            if (group != null) {
                item.fillItemGroup(group, stacks);
            }
            matches.addAll(stacks.stream().filter(predicate).collect(Collectors.toList()));
        }
        matchingStacks = matches.toArray(matchingStacks);
        matchingStacksCached = true;
    }


}
