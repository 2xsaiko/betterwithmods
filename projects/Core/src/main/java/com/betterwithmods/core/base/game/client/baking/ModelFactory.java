/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.client.baking;

import com.betterwithmods.core.utilties.ModelUtils;
import com.google.common.collect.ImmutableList;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemOverrideList;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelProperty;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public abstract class ModelFactory<T> extends BaseBakedModel {

    private final ModelProperty<T> property;
    private ResourceLocation particle;

    public ModelFactory(ModelProperty<T> property, ResourceLocation particle) {
        this.property = property;
        this.particle = particle;
    }

    public abstract IBakedModel bake(@Nonnull ModelInfo<T> info);

    public abstract ModelInfo<T> fromItemStack(ItemStack stack);

    @Nonnull
    @Override
    public List<BakedQuad> getQuads(@Nullable IBlockState state, @Nullable EnumFacing side, @Nonnull Random rand, @Nonnull IModelData extraData) {
        IBakedModel model = bake(new ModelInfo<>(state, data -> data.getData(property), false));
        if (model != null) {
            return model.getQuads(state, side, rand, extraData);
        }
        return ImmutableList.of();
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return ModelUtils.getSprite(particle);
    }

    protected static class FactoryItemOverrideList<T> extends ItemOverrideList {

        public FactoryItemOverrideList() {
            super();
        }

        @Nullable
        @Override
        public IBakedModel getModelWithOverrides(@Nonnull IBakedModel model, @Nonnull ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn) {
            if (model instanceof ModelFactory) {
                ModelFactory<T> factory = (ModelFactory<T>) model;
                ModelInfo<T> info = factory.fromItemStack(stack);
                if (info != null) {
                    return factory.bake(info);
                }
            }
            return model;
        }
    }


}
