/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import com.betterwithmods.core.utilties.inventory.InventoryUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.Tag;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraftforge.items.IItemHandler;

import java.util.Optional;

public class PlayerUtils {

    public static ItemStack getHeld(EntityPlayer player, EnumHand hand) {
        return player.getHeldItem(hand);
    }

    public static boolean emptyHands(EntityPlayer player) {
        return getHeld(player, EnumHand.MAIN_HAND).isEmpty() && getHeld(player, EnumHand.OFF_HAND).isEmpty();
    }


    public static boolean isHolding(EntityPlayer player, Ingredient ingredient) {
        return ingredient.test(getHeld(player, EnumHand.MAIN_HAND)) || ingredient.test(getHeld(player, EnumHand.OFF_HAND));
    }

    public static boolean isHolding(EntityPlayer player, Tag<Item> tag) {
        return isHolding(player, Ingredient.fromTag(tag));
    }


    //Main from vertical, Equipment from horizontal, null is joined
    public static IItemHandler getMainInventory(EntityPlayer player) {
        return InventoryUtils.getInventory(player, EnumFacing.UP);
    }

    public static IItemHandler getEquipmentInventory(EntityPlayer player) {
        return InventoryUtils.getInventory(player, EnumFacing.NORTH);
    }

    public static IItemHandler getJoinedInventory(EntityPlayer player) {
        return InventoryUtils.getInventory(player, null);
    }

    public static Optional<ItemStack> findInPlayer(EntityPlayer player, Ingredient ingredient, boolean extract) {
        return InventoryUtils.findInInventory(getJoinedInventory(player), ingredient, extract);
    }

    public static void givePlayer(EntityPlayer player, ItemStack stack) {
        InventoryUtils.addToInventory(getMainInventory(player), stack);
    }


}
