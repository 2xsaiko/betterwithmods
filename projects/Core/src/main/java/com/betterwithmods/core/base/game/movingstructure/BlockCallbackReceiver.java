package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Implement this on Block to receive callbacks from moving structures
 */
public interface BlockCallbackReceiver {

    void onCompleted(World world, BlockPos pos, MovingStructure structure);

    void onBlocked(World world, BlockPos pos, MovingStructure structure);

}
