/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ConfigUtils {

    public static boolean validateStringToEntityClass(Object element, Class<? extends Entity> parent) {
        if (element instanceof String) {
            String id = (String) element;
            EntityType<?> type = EntityType.getById(id);
            if (type != null) {
                Class<? extends Entity> clazz = type.getEntityClass();
                return parent.isAssignableFrom(clazz);
            }
        }
        return false;
    }

    public static boolean validateStringToItemClass(Object element) {
        if (element instanceof String) {
            String id = (String) element;
            return ForgeRegistries.ITEMS.getValue(new ResourceLocation(id)) != null;
        }

        return false;
    }

    public static Ingredient getIngredientFromStringList(List<? extends String> itemNames) {
        List<ItemStack> items = itemNames.stream()
                .map(ResourceLocation::new)
                .map(ForgeRegistries.ITEMS::getValue)
                .map(ItemStack::new)
                .collect(Collectors.toList());

        return Ingredient.fromStacks(items.toArray(new ItemStack[0]));
    }


    public static List<EntityType<?>> convertStringsToEntityType(List<? extends String> ids) {
        return ids.stream()
                .map(EntityType::getById)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static <T extends Entity> List<Class<? extends T>> convertStringsToEntityClass(List<? extends String> ids, Class<T> clazz) {
        return convertStringsToEntityType(ids).stream()
                .map(EntityType::getEntityClass)
                .map(e -> (Class<T>) e)
                .collect(Collectors.toList());
    }

    public static List<Class<? extends EntityCreature>> getEntities(ForgeConfigSpec.ConfigValue<List<? extends String>> value) {
        return convertStringsToEntityClass(value.get(), EntityCreature.class);
    }
}
