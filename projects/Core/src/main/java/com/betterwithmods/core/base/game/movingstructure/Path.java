package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

public interface Path {

    Vec3d getOffset(int ticks);

    int maxTime();

    PathType type();

    /**
     * Call {@link PathType#save(Path)} for saving
     */
    void save(NBTTagCompound nbt);

}
