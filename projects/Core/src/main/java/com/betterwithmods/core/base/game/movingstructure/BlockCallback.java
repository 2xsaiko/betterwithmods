package com.betterwithmods.core.base.game.movingstructure;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

import java.util.Objects;

public class BlockCallback implements Callback {

    private final World world;
    private final BlockPos pos;

    public BlockCallback(World world, BlockPos pos) {
        this.world = world;
        this.pos = pos;
    }

    @Override
    public void onCompleted(MovingStructure structure) {
        Block block = world.getBlockState(pos).getBlock();
        if (block instanceof BlockCallbackReceiver) {
            ((BlockCallbackReceiver) block).onCompleted(world, pos, structure);
        }
    }

    @Override
    public void onBlocked(MovingStructure structure) {
        Block block = world.getBlockState(pos).getBlock();
        if (block instanceof BlockCallbackReceiver) {
            ((BlockCallbackReceiver) block).onBlocked(world, pos, structure);
        }
    }

    @Override
    public CallbackType type() {
        return CallbackType.Block;
    }

    @Override
    public void save(NBTTagCompound nbt) {
        nbt.setString("dimension", Objects.requireNonNull(DimensionType.func_212678_a(world.getDimension().getType())).toString());
        nbt.setInt("x", pos.getX());
        nbt.setInt("y", pos.getY());
        nbt.setInt("z", pos.getZ());
    }

    public static BlockCallback load(NBTTagCompound nbt) {
        DimensionType dt = DimensionType.byName(new ResourceLocation(nbt.getString("dimension")));
        World world = ServerLifecycleHooks.getCurrentServer().getWorld(Objects.requireNonNull(dt));
        BlockPos pos = new BlockPos(nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z"));
        return new BlockCallback(world, pos);
    }

}
