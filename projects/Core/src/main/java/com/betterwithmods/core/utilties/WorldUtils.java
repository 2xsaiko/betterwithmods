/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.BlockRedstoneWire;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.EnumLightType;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import java.lang.reflect.Field;

public class WorldUtils {

    public static Vec3d fromContext(ItemUseContext context) {
        return new Vec3d(context.getHitX(), context.getHitY(), context.getHitZ());
    }

    public static Vec3d fromEntity(Entity entity) {
        return new Vec3d(entity.posX, entity.posY, entity.posZ);
    }

    public static int getWorldUpdateLCG(World world) {
        try {
            Field field = world.getClass().getDeclaredField("updateLCG");
            return (int) field.get(world);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }


    public static boolean isValidForSnow(World world, BlockPos pos) {
        Biome biome = world.getBiome(pos);
        if (biome.getTemperature(pos) >= 0.15F) {
            return false;
        }
        return pos.getY() >= 0 && pos.getY() < 256 && world.getLightFor(EnumLightType.BLOCK, pos) < 10;

    }

    public static boolean isPowered(World world, BlockPos pos) {
        return calculateNeighborStrength(world,pos, world.getBlockState(pos)) > 0;
    }

    public static int calculateNeighborStrength(World world, BlockPos pos, IBlockState state) {
        int max = 0;
        for(EnumFacing facing: FacingUtils.FACINGS) {
            int i = calculateInputStrength(world,pos, state,facing);
            if(i > max) {
                max = i;
            }
        }
        return max;
    }

    public static int calculateInputStrength(World worldIn, BlockPos pos, IBlockState state, EnumFacing facing) {
        BlockPos blockpos = pos.offset(facing);
        int i = worldIn.getRedstonePower(blockpos, facing);
        if (i > 0) {
            return i;
        } else {
            IBlockState iblockstate = worldIn.getBlockState(blockpos);
            return Math.max(i, iblockstate.getBlock() == Blocks.REDSTONE_WIRE ? iblockstate.get(BlockRedstoneWire.POWER) : 0);
        }
    }


}

