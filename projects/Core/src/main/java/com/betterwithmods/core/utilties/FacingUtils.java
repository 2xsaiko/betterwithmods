/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.utilties;

import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.function.Predicate;

public class FacingUtils {

    private static final EnumFacing[][] AXIS_TO_FACING = new EnumFacing[][]{
            new EnumFacing[]{EnumFacing.WEST, EnumFacing.EAST},
            new EnumFacing[]{EnumFacing.UP, EnumFacing.DOWN},
            new EnumFacing[]{EnumFacing.NORTH, EnumFacing.SOUTH}
    };

    public static EnumFacing[] FACINGS = EnumFacing.values();

    public static Iterable<EnumFacing> fromAxis(EnumFacing.Axis axis) {
        if (axis != null)
            return Arrays.asList(AXIS_TO_FACING[axis.ordinal()]);
        return Collections.emptyList();
    }

    public static EnumSet<EnumFacing> FACING_SET = EnumSet.allOf(EnumFacing.class);

    public static EnumSet<EnumFacing> HORIZONTAL_SET = EnumSet.complementOf(EnumSet.of(EnumFacing.DOWN, EnumFacing.UP));

    public static Iterable<EnumFacing> excluding(EnumFacing facing) {
        return EnumSet.complementOf(EnumSet.of(facing));
    }

    public static Iterable<EnumFacing> excluding(EnumFacing f, EnumFacing... f2) {
        return EnumSet.complementOf(EnumSet.of(f, f2));
    }

    public static boolean isFaceShapeInDirection(World world, BlockPos pos, EnumFacing facing, Predicate<BlockFaceShape> predicate) {
        BlockPos offsetPos = pos.offset(facing);
        IBlockState offsetState = world.getBlockState(offsetPos);
        BlockFaceShape shape = offsetState.getBlockFaceShape(world, offsetPos, facing.getOpposite());
        return predicate.test(shape);
    }


    public static void write(NBTTagCompound compound, String tag, @Nullable EnumFacing facing) {
        if (facing != null) {
            compound.setInt(tag, facing.getIndex());
        }
    }

    @Nullable
    public static EnumFacing read(NBTTagCompound compound, String tag) {
        if (compound.hasKey(tag)) {
            return FACINGS[compound.getInt(tag)];
        }
        return null;
    }

}
