/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import com.google.common.collect.Maps;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.function.Supplier;

public class ModuleLoader extends SetupLoader {

    public final Map<String, Supplier<Boolean>> JSON_CONDITIONS = Maps.newHashMap();

    public Logger logger;
    public String modid;

    public ModuleLoader(String modid) {
        this.modid = modid;
        CraftingHelper.register(new ResourceLocation(modid, "config"), json -> {
            String enabled = json.get("enabled").getAsString();
            return () -> JSON_CONDITIONS.getOrDefault(enabled, () -> false).get();
        });
    }

    public void addModule(ModuleBase module) {
        this.addSetup(module);
        module.setLoader(this);
    }

    public void addCondition(String name, Supplier<Boolean> condition) {
        JSON_CONDITIONS.put(name, condition);
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public Logger getLogger() {
        return logger;
    }
}
