/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.game.recipes;

import com.betterwithmods.core.Core;
import com.betterwithmods.core.References;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Random;

public class ToolRecipe implements IRecipe {

    private final ResourceLocation id;
    private final String group;

    private final Ingredient tool;
    private final Ingredient input;

    private final ItemStack result;

    private final NonNullList<ItemStack> additionalDrops;

    private boolean damageTool;

    private SoundEvent sound;

    public ToolRecipe(ResourceLocation id, String group, Ingredient tool, Ingredient input, ItemStack result, NonNullList<ItemStack> additionalDrops, SoundEvent sound, boolean damageTool) {
        this.id = id;
        this.group = group;
        this.tool = tool;
        this.input = input;
        this.result = result;
        this.additionalDrops = additionalDrops;
        this.sound = sound;
        this.damageTool = damageTool;
        if (damageTool) {
            MinecraftForge.EVENT_BUS.register(this);
        }
    }

    @Override
    public boolean matches(IInventory inv, World worldIn) {
        return isMatch(inv, worldIn);
    }

    public boolean isMatch(IInventory inv, World world) {
        boolean hasTool = false, hasInput = false;
        for (int x = 0; x < inv.getSizeInventory(); x++) {
            boolean inRecipe = false;
            ItemStack slot = inv.getStackInSlot(x);

            if (!slot.isEmpty()) {
                if (tool.test(slot)) {
                    if (!hasTool) {
                        hasTool = true;
                        inRecipe = true;
                    } else {
                        return false;
                    }
                } else if (input.test(slot)) {
                    if (!hasInput) {
                        hasInput = true;
                        inRecipe = true;
                    } else {
                        return false;
                    }
                }
                if (!inRecipe)
                    return false;
            }
        }
        return hasTool && hasInput;
    }


    @Override
    public ItemStack getCraftingResult(IInventory inv) {
        return result.copy();
    }

    @Override
    public boolean canFit(int width, int height) {
        return width * height >= 2;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return result.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return Core.TOOL_RECIPE;
    }

    @Override
    public NonNullList<ItemStack> getRemainingItems(IInventory inv) {
        NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);
        for (int i = 0; i < nonnulllist.size(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if (tool.test(item)) {
                if (damageTool) {
                    item.attemptDamageItem(1, new Random(), null);
                }
                nonnulllist.set(i, item.copy());
            } else if (item.hasContainerItem()) {
                nonnulllist.set(i, item.getContainerItem());
            }
        }
        return nonnulllist;
    }

    @Override
    public String getGroup() {
        return group;
    }


    public static class Serializer implements IRecipeSerializer<ToolRecipe> {

        private static final ResourceLocation NAME = new ResourceLocation(References.MODID_CORE, "tool_recipe");

        @Override
        public ToolRecipe read(ResourceLocation recipeId, JsonObject json) {
            String group = JsonUtils.getString(json, "group", "");
            Ingredient tool = Ingredient.fromJson(json.get("tool"));
            Ingredient input = Ingredient.fromJson(json.get("input"));
            ItemStack result = ShapedRecipe.deserializeItem(JsonUtils.getJsonObject(json, "result"));
            boolean damageTool = JsonUtils.getBoolean(json, "damageTool", false);

            NonNullList<ItemStack> additionalDrops = NonNullList.create();
            if (json.has("additionalDrops")) {
                for (JsonElement dropElement : JsonUtils.getJsonArray(json, "additionalDrops")) {
                    ItemStack drop = ShapedRecipe.deserializeItem(dropElement.getAsJsonObject());
                    additionalDrops.add(drop);
                }
            }

            String soundName = JsonUtils.getString(json, "sound", "");
            SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(soundName));

            return new ToolRecipe(recipeId, group, tool, input, result, additionalDrops, sound, damageTool);
        }

        @Override
        public ToolRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            String group = buffer.readString(Short.MAX_VALUE);
            Ingredient tool = Ingredient.fromBuffer(buffer);
            Ingredient input = Ingredient.fromBuffer(buffer);
            ItemStack result = buffer.readItemStack();
            boolean damageTool = buffer.readBoolean();
            NonNullList<ItemStack> additionalDrops = NonNullList.create();
            int count = buffer.readInt();
            for (int i = 0; i < count; count++) {
                additionalDrops.add(buffer.readItemStack());
            }
            String soundName = buffer.readString(Short.MAX_VALUE);
            SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(soundName));
            return new ToolRecipe(recipeId, group, tool, input, result, additionalDrops, sound, damageTool);
        }

        @Override
        public void write(PacketBuffer buffer, ToolRecipe recipe) {
            buffer.writeString(recipe.getGroup());
            recipe.tool.writeToBuffer(buffer);
            recipe.input.writeToBuffer(buffer);
            buffer.writeItemStack(recipe.result);
            buffer.writeBoolean(recipe.damageTool);
            if (!recipe.additionalDrops.isEmpty()) {
                buffer.writeInt(recipe.additionalDrops.size());
                for (ItemStack drop : recipe.additionalDrops) {
                    buffer.writeItemStack(drop);
                }
            }
            if (recipe.sound != null) {
                buffer.writeString(recipe.sound.getRegistryName().toString());
            } else {
                buffer.writeString("");
            }
        }

        @Override
        public ResourceLocation getName() {
            return NAME;
        }
    }

    public void playSound(World world, BlockPos pos) {
        if(sound != null) {
            EnvironmentUtils.playSound(world, pos, sound, SoundCategory.PLAYERS, 0.25F, 2.5F);
        }
    }

    @SubscribeEvent
    public void dropExtra(PlayerEvent.ItemCraftedEvent event) {
        if (event.getPlayer() == null)
            return;
        EntityPlayer player = event.getPlayer();
        World world = player.getEntityWorld();
        if (isMatch(event.getInventory(), world)) {
            playSound(world, player.getPosition());
            if (!world.isRemote) {
                for (ItemStack drops : additionalDrops) {
                    player.entityDropItem(drops.copy());
                }
            }
        }
    }

}
