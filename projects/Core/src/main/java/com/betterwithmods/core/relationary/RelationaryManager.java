/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.resources.IResource;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.resource.IResourceType;
import net.minecraftforge.resource.ISelectiveResourceReloadListener;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RelationaryManager implements ISelectiveResourceReloadListener {

    public static final Map<ResourceLocation, Relation> relations = Maps.newHashMap();
    public static final Map<ResourceLocation, Shape> shapes = Maps.newHashMap();

    private static final Pattern SHAPES_PATTERN = Pattern.compile("shapes/(.*).json");
    private static final Pattern RELATIONS_PATTERN = Pattern.compile("relations/(.*).json");

    protected static final Multimap<HashableItemStack, Relation> ITEM_STACK_TO_RELATION = MultimapBuilder.hashKeys().hashSetValues().build();

    private Logger logger;

    public RelationaryManager(Logger logger) {
        this.logger = logger;
    }

    public static Relation getOrCreateRelation(ResourceLocation registryName) {
        return relations.getOrDefault(registryName, new Relation(registryName));
    }

    public static Set<Relation> getRelations(ItemStack stack) {
        HashableItemStack hashed = new HashableItemStack(stack);
        return (Set<Relation>) ITEM_STACK_TO_RELATION.get(hashed);
    }

    public static Shape getOrCreateShape(ResourceLocation registryName) {
        return shapes.getOrDefault(registryName, new Shape(registryName, ""));
    }

    public static Set<Shape> getShape(Relation relation, ItemStack stack) {
        return (Set<Shape>) relation.fromStack(stack);
    }

    public static boolean isShape(Relation relation, ItemStack stack, Shape shape) {
        return getShape(relation, stack).contains(shape);
    }

    @Nullable
    public static Relation getRelationWithShape(ItemStack stack, Shape shape) {
        Set<Relation> relations = getRelations(stack);
        for (Relation relation : relations) {
            if (isShape(relation, stack, shape)) {
                return relation;
            }
        }
        return null;
    }

    private void addShape(Shape shape) {
        if (shapes.containsKey(shape.getRegistryName())) {
            throw new IllegalStateException("Duplicate shape ignored with ID " + shape.getRegistryName());
        } else {
            shapes.put(shape.getRegistryName(), shape);
        }
    }

    private void addRelation(Relation relation) {
        if (relations.containsKey(relation.getRegistryName())) {
            throw new IllegalStateException("Duplicate relation ignored with ID " + relation.getRegistryName());
        } else {
            relations.put(relation.getRegistryName(), relation);
        }
    }

    private ResourceLocation stripPath(Pattern pathPattern, ResourceLocation location) {
        String fullPath = location.getPath();
        Matcher matcher = pathPattern.matcher(fullPath);
        if (matcher.matches())
            return new ResourceLocation(location.getNamespace(), matcher.group(1));
        return null;
    }

    @Override
    public void onResourceManagerReload(IResourceManager resourceManager, Predicate<IResourceType> resourcePredicate) {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
        relations.clear();
        shapes.clear();

        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("shapes", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = stripPath(SHAPES_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try (IResource iresource = resourceManager.getResource(resourcelocation)) {
                JsonObject jsonobject = JsonUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                if (jsonobject != null) {
                    addShape(Shape.deserialize(registryName, jsonobject));
                } else {
                    logger.error("Couldn't load shape {} as it's null or empty", registryName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Map<ResourceLocation, Relation> relations = Maps.newHashMap();
        for (ResourceLocation resourcelocation : resourceManager.getAllResourceLocations("relations", (file) -> {
            return file.endsWith(".json") && !file.startsWith("_"); //Forge filter anything beginning with "_" as it's used for metadata.
        })) {
            ResourceLocation registryName = stripPath(RELATIONS_PATTERN, resourcelocation);
            if (registryName == null) {
                logger.error("Failed to parse registryName for {}", resourcelocation);
                continue;
            }
            try {
                for (IResource iresource : resourceManager.getAllResources(resourcelocation)) {
                    JsonObject jsonobject = JsonUtils.fromJson(gson, IOUtils.toString(iresource.getInputStream(), StandardCharsets.UTF_8), JsonObject.class);
                    if (jsonobject != null) {
                        Relation relation = relations.getOrDefault(registryName, new Relation(registryName));
                        Relation.deserialize(relation, jsonobject);
                        relations.put(registryName, relation);
                    } else {
                        logger.error("Couldn't load relation {} as it's null or empty", registryName);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        relations.values().forEach(this::addRelation);

        Relation.VERSION++;
    }
}

