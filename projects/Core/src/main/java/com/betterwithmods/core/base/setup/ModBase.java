/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.base.setup;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.base.setup.proxy.IProxy;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;
import net.minecraftforge.fml.ExtensionPoint;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.FMLPlayMessages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class ModBase extends ModuleLoader {

    private final Logger logger;
    private final String modid, name;

    protected IProxy proxy;

    public ModBase(String modid, String name) {
        super(modid);
        this.name = name;
        this.modid = modid;
        this.logger = LogManager.getLogger(name);

        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onCommonSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onClientSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::onLoadComplete);
        ModLoadingContext.get().registerExtensionPoint(ExtensionPoint.GUIFACTORY, () -> this::getClientContainer);
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        getLogger().info("common setup");
        super.onCommonSetup(event);
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        getLogger().info("client setup");
        super.onClientSetup(event);
    }

    @Override
    public void onLoadComplete(FMLLoadCompleteEvent event) {
        getLogger().info("load complete");
        super.onLoadComplete(event);
    }

    public void addProxy(IProxy proxy) {
        this.proxy = proxy;
        addSetup(proxy);
    }

    public GuiScreen getClientContainer(FMLPlayMessages.OpenContainer packet) {
        PacketBuffer buf = packet.getAdditionalData();
        BlockPos pos = buf.readBlockPos();
        World world = proxy.getClientWorld();
        EntityPlayer player = proxy.getClientPlayer();
        IBlockState state = world.getBlockState(pos);
        Block block = state.getBlock();
        if (block instanceof BlockBase) {
            BlockBase base = ((BlockBase) block);
            IInteractionObject interaction = base.getInteractionObject(world, pos);
            return base.getScreen(world, pos, player, interaction);
        }
        return null;
    }
}
