/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.core.relationary;

import com.google.gson.JsonObject;
import net.minecraft.util.ResourceLocation;

public class Shape {

    private ResourceLocation registryName;
    private String name;

    protected static int VERSION;

    public Shape(ResourceLocation registryName, String name) {
        this.registryName = registryName;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return getRegistryName().hashCode();
    }


    @Override
    public boolean equals(Object o) {
        if(o instanceof Shape) {
            return ((Shape) o).getRegistryName().equals(this.getRegistryName());
        }
        return false;
    }

    @Override
    public String toString() {
        return getRegistryName().toString();
    }

    public ResourceLocation getRegistryName() {
        return registryName;
    }

    public String getName() {
        return name;
    }

    public static Shape deserialize(ResourceLocation registryName, JsonObject json) {
        String name = json.get("name").getAsString();
        return new Shape(registryName, name);
    }

    public static class Wrapper extends Shape {
        private int lastVersion = -1;

        private Shape cachedShape;

        public Wrapper(ResourceLocation id) {
            super(id, "");
        }

        private void validateCache() {
            if (lastVersion != VERSION) {
                this.cachedShape = RelationaryManager.getOrCreateShape(getRegistryName());
                this.lastVersion = VERSION;
            }
        }
    }
}
