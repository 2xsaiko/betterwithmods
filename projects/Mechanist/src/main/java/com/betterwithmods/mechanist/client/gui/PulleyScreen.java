/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.client.gui;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.client.gui.BaseContainerScreen;
import com.betterwithmods.mechanist.content.container.PulleyContainer;
import com.betterwithmods.mechanist.content.tiles.mechanical.TilePulley;
import net.minecraft.util.ResourceLocation;

public class PulleyScreen extends BaseContainerScreen<PulleyContainer, TilePulley> {

    private static final ResourceLocation PULLEY_BACKGROUND = new ResourceLocation(References.MODID_MECHANIST, "textures/gui/pulley/background.png");

    public PulleyScreen(PulleyContainer container, TilePulley tile) {
        super(container, tile, PULLEY_BACKGROUND);
        this.setHeight(175);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        drawName(tile::getName, 6, 8, 94);
    }

}
