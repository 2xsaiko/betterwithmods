/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.hemp;

import com.betterwithmods.core.base.game.block.plant.BlockPlant;
import com.betterwithmods.core.base.game.block.plant.GrowthConditions;
import com.betterwithmods.core.utilties.Rand;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Items;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;

import java.util.Random;

public class BlockHempBottom extends BlockPlant {

    private static VoxelShape[] SHAPE_BY_AGE = new VoxelShape[]{
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 2.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 4.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 6.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 8.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 10.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 12.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 14.0D, 12.0D),
            Block.makeCuboidShape(4.0D, 0.0D, 4.0D, 12.0D, 16.0D, 12.0D)
    };



    public BlockHempBottom(Block.Properties builder) {
        super(builder, 7);
        this.setDefaultState(this.getDefaultState().with(getAgeProperty(), 0));

        growthConditionReducer(GrowthConditions.AND);
        growthConditions(
                GrowthConditions.aggregate(GrowthConditions.OR, GrowthConditions::hasSun, GrowthConditions::hasLamp),
                GrowthConditions::isFertileGround,
                new GrowthConditions.LightLevel(12)
        );
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return SHAPE_BY_AGE[state.get(this.getAgeProperty())];
    }

    @Override
    public void grow(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        if (isMaxAge(state) && !hasTopStage(worldIn, pos)) {
            worldIn.setBlockState(pos.up(), Registrars.Blocks.HEMP_PLANT_TOP.getDefaultState(), 2);
        }
    }

    public boolean hasTopStage(IBlockReader worldIn, BlockPos pos) {
        IBlockState above = worldIn.getBlockState(pos.up());
        return above.getBlock() == Registrars.Blocks.HEMP_PLANT_TOP;
    }

    @Override
    public boolean canGrow(IBlockReader worldIn, BlockPos pos, IBlockState state, boolean isClient) {
        if (isMaxAge(state) && hasTopStage(worldIn, pos))
            return false;
        return isSatisfied((IWorldReaderBase) worldIn, pos, state);
    }

    @Override
    public IItemProvider getSeed() {
        return Registrars.Items.HEMP_SEED;
    }

    @Override
    public IItemProvider getCrop() {
        return Items.AIR;
    }

    @Override
    public IntegerProperty getAgeProperty() {
        return BlockStateProperties.AGE_0_7;
    }

    @Override
    public boolean shouldGrow(World worldIn, BlockPos pos) {
        return Rand.chance(worldIn.rand, 0.1);
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, IBlockState state) {
        return false;
    }
}
