/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.client.entities;

import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.blocks.BlockMiningCharge;
import com.betterwithmods.mechanist.content.entities.EntityMiningCharge;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class RenderMiningCharge extends Render<EntityMiningCharge> {

    public RenderMiningCharge(RenderManager manager) {
        super(manager);
        this.shadowSize = 0.5F;
    }

    @Override
    public void doRender(EntityMiningCharge entity, double x, double y, double z, float d, float t) {
        BlockRendererDispatcher dispatcher = Minecraft.getInstance().getBlockRendererDispatcher();
        GlStateManager.pushMatrix();
        GlStateManager.translatef((float) x, (float) y + 0.5F, (float) z);

        //Pulse scale
        float scale;
        if ((float) entity.getFuse() - t + 1.0F < 10.0F) {
            scale = 1.0F - ((float) entity.getFuse() - t + 1.0F) / 10.0F;
            scale = MathHelper.clamp(scale, 0.0F, 1.0F);
            scale *= scale;
            scale *= scale;
            float lvt_12_1_ = 1.0F + scale * 0.3F;
            GlStateManager.scalef(lvt_12_1_, lvt_12_1_, lvt_12_1_);
        }

        scale = (1.0F - ((float) entity.getFuse() - t + 1.0F) / 100.0F) * 0.8F;
        this.bindTexture(getEntityTexture(entity));

        EnumFacing facing = entity.getFacing();
        IBlockState MINING_CHARGE = Registrars.Blocks.MINING_CHARGE.getDefaultState().with(BlockMiningCharge.FACING, facing);

        GlStateManager.rotatef(-90.0F, 0.0F, 1.0F, 0.0F);
        GlStateManager.translatef(-0.5F, -0.5F, 0.5F);
        dispatcher.renderBlockBrightness(MINING_CHARGE, entity.getBrightness());
        GlStateManager.translatef(0.0F, 0.0F, 1.0F);

        if (this.renderOutlines) {
            GlStateManager.enableColorMaterial();
            GlStateManager.enableOutlineMode(this.getTeamColor(entity));
            dispatcher.renderBlockBrightness(MINING_CHARGE, 1.0F);
            GlStateManager.disableOutlineMode();
            GlStateManager.disableColorMaterial();
        } else if (entity.getFuse() / 5 % 2 == 0) {

            if (facing.getAxis().isHorizontal()) {
                GlStateManager.rotatef(-90.0F, 0.0F, 1.0F, 0.0F);
                GlStateManager.translatef(-1F, 0, 0);
            }

            GlStateManager.disableTexture2D();
            GlStateManager.disableLighting();
            GlStateManager.enableBlend();
            GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.DST_ALPHA);
            GlStateManager.color4f(1.0F, 1.0F, 1.0F, scale);
            GlStateManager.polygonOffset(-3.0F, -3.0F);
            GlStateManager.enablePolygonOffset();
            dispatcher.renderBlockBrightness(MINING_CHARGE, 1.0F);
            GlStateManager.polygonOffset(0.0F, 0.0F);
            GlStateManager.disablePolygonOffset();
            GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.disableBlend();
            GlStateManager.enableLighting();
            GlStateManager.enableTexture2D();
        }

        GlStateManager.popMatrix();
        super.doRender(entity, x, y, z, d, t);
    }

    @Nonnull
    @Override
    protected ResourceLocation getEntityTexture(EntityMiningCharge entity) {
        return TextureMap.LOCATION_BLOCKS_TEXTURE;
    }
}
