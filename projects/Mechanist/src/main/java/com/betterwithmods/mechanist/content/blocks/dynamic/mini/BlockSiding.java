/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.dynamic.mini;

import com.betterwithmods.mechanist.content.blocks.dynamic.BlockDynWaterRotate;
import net.minecraft.block.Block;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockSiding extends BlockDynWaterRotate {

    protected static final DirectionProperty FACING = BlockStateProperties.FACING;

    public BlockSiding(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, EnumFacing.UP));
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState()
                .with(WATERLOGGED, shouldWaterlog(context))
                .with(FACING, context.getFace());
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockReader worldIn, IBlockState state, BlockPos pos, EnumFacing face) {
        if (state.get(FACING) == face)
            return BlockFaceShape.SOLID;
        return BlockFaceShape.UNDEFINED;
    }

    public IBlockState rotate(IBlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Override
    public void onRotate(World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.cycle(FACING));
    }



    private static final VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 8, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 8, 16),
            Block.makeCuboidShape(0, 0, 8, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 8),
            Block.makeCuboidShape(8, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 8, 16, 16),
    };

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        int i = state.get(FACING).getIndex();
        return SHAPES[i];
    }



}
