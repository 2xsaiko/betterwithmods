/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.core.utilties.ReflectionHelper;
import com.google.common.collect.ImmutableMap;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFlowingFluid;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.fluid.FlowingFluid;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.IFluidState;
import net.minecraft.init.Blocks;
import net.minecraft.init.Fluids;
import net.minecraft.init.Particles;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import javax.annotation.Nullable;

public class StrategicWater extends ModuleBase {

    public StrategicWater() {
        super("strategic_water");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
    }


    private static final ImmutableMap<Block, IBucketPickupHandler> BUCKET_HANDLER_OVERRIDES =
            ImmutableMap.<Block, IBucketPickupHandler>builder()
                    .put(Blocks.WATER, new BucketHandlerWrapper(Blocks.WATER))
                    .build();

    private static final int BUCKET_COOLDOWN = 20;

    @SubscribeEvent
    public void fillBucket(FillBucketEvent event) {
        if (event.getResult() == Event.Result.DENY)
            return;
        RayTraceResult target = event.getTarget();
        ItemStack current = event.getEmptyBucket(); //Not necessarily empty, just what the event calls initial bucket
        World world = event.getWorld();
        EntityPlayer player = event.getEntityPlayer();

        ItemBucket bucket = null;
        if (current.getItem() instanceof ItemBucket) {
            bucket = (ItemBucket) current.getItem();
        }

        if (bucket == null) {
            return;
        }

        Fluid containedBlock = bucket.containedBlock;

        if(player.getCooldownTracker().hasCooldown(bucket))
            return;

        if (target != null && target.type == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = target.getBlockPos();
            if (world.isBlockModifiable(player, blockpos) && player.canPlayerEdit(blockpos, target.sideHit, current)) {
                if (containedBlock == Fluids.EMPTY) {
                    IBlockState state = world.getBlockState(blockpos);
                    ItemStack result = tryPickupLiquid(bucket, world, blockpos, state, player, current);
                    if (result == null) {
                        BlockPos offset = blockpos.offset(target.sideHit);
                        IBlockState offsetState = world.getBlockState(offset);
                        result = tryPickupLiquid(bucket, world, offset, offsetState, player, current);
                    }
                    if (result != null) {
                        event.setResult(Event.Result.ALLOW);
                        event.setFilledBucket(result);
                        player.getCooldownTracker().setCooldown(result.getItem(), BUCKET_COOLDOWN);
                    }
                } else {
                    IBlockState iblockstate = world.getBlockState(blockpos);
                    BlockPos blockpos1 = bucket.getPlacementPosition(iblockstate, blockpos, target);
                    if (tryPlaceContainedLiquid(bucket, player, world, blockpos1, target)) {
                        bucket.onLiquidPlaced(world, current, blockpos1);
                        if (player instanceof EntityPlayerMP) {
                            CriteriaTriggers.PLACED_BLOCK.trigger((EntityPlayerMP) player, blockpos1, current);
                        }
                        ItemStack result = bucket.emptyBucket(current, player);
                        player.addStat(StatList.ITEM_USED.get(bucket));
                        event.setResult(Event.Result.ALLOW);
                        event.setFilledBucket(result);
                        player.getCooldownTracker().setCooldown(result.getItem(), BUCKET_COOLDOWN);
                    }
                }
            }

        }

    }


    public static ItemStack tryPickupLiquid(ItemBucket bucket, World world, BlockPos blockpos, IBlockState state, EntityPlayer player, ItemStack current) {
        if (state.getBlock() instanceof IBucketPickupHandler) {
            IBucketPickupHandler handler = ((IBucketPickupHandler) state.getBlock());
            Fluid fluid = BUCKET_HANDLER_OVERRIDES.getOrDefault(state.getBlock(), handler).pickupFluid(world, blockpos, state);

            if (fluid != Fluids.EMPTY) {
                player.addStat(StatList.ITEM_USED.get(bucket));
                player.playSound(fluid.isIn(FluidTags.LAVA) ? SoundEvents.ITEM_BUCKET_FILL_LAVA : SoundEvents.ITEM_BUCKET_FILL, 1.0F, 1.0F);
                ItemStack result = bucket.fillBucket(current, player, fluid.getFilledBucket());
                if (!world.isRemote) {
                    CriteriaTriggers.FILLED_BUCKET.func_204817_a((EntityPlayerMP) player, new ItemStack(fluid.getFilledBucket()));
                }
                return result;
            }
        }
        return null;
    }

    public static boolean tryPlaceContainedLiquid(ItemBucket bucket, @Nullable EntityPlayer player, World worldIn, BlockPos posIn, @Nullable RayTraceResult target) {
        Fluid containedBlock = bucket.containedBlock;
        if (!(containedBlock instanceof FlowingFluid)) {
            return false;
        } else {
            IBlockState iblockstate = worldIn.getBlockState(posIn);
            Material material = iblockstate.getMaterial();
            boolean notSolid = !material.isSolid();
            boolean replaceable = material.isReplaceable();
            if (worldIn.isAirBlock(posIn) || notSolid || replaceable || iblockstate.getBlock() instanceof ILiquidContainer && ((ILiquidContainer) iblockstate.getBlock()).canContainFluid(worldIn, posIn, iblockstate, containedBlock)) {
                IFluidState fluidState = ((FlowingFluid) containedBlock).getFlowingFluidState(4, false);
                if (worldIn.dimension.doesWaterVaporize() && containedBlock.isIn(FluidTags.WATER)) {
                    int i = posIn.getX();
                    int j = posIn.getY();
                    int k = posIn.getZ();
                    worldIn.playSound(player, posIn, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, 2.6F + (worldIn.rand.nextFloat() - worldIn.rand.nextFloat()) * 0.8F);

                    for (int l = 0; l < 8; ++l) {
                        worldIn.spawnParticle(Particles.LARGE_SMOKE, (double) i + Math.random(), (double) j + Math.random(), (double) k + Math.random(), 0.0D, 0.0D, 0.0D);
                    }
                } else if (iblockstate.getBlock() instanceof ILiquidContainer) {
                    if (((ILiquidContainer) iblockstate.getBlock()).receiveFluid(worldIn, posIn, iblockstate, fluidState)) {
                        bucket.playEmptySound(player, worldIn, posIn);
                    }
                } else {
                    if (!worldIn.isRemote && (notSolid || replaceable) && !material.isLiquid()) {
                        worldIn.destroyBlock(posIn, true);
                    }
                    bucket.playEmptySound(player, worldIn, posIn);
                    worldIn.setBlockState(posIn, fluidState.getBlockState(), 11);
                    for (EnumFacing facing : FacingUtils.HORIZONTAL_SET) {
                        BlockPos pos = posIn.offset(facing);
                        IBlockState blockState = worldIn.getBlockState(pos);
                        IFluidState fluidState1 = worldIn.getFluidState(pos);
                        if(blockState.getMaterial().isReplaceable() && fluidState1.isEmpty()) {
                            worldIn.setBlockState(pos, fluidState.getBlockState(), 11);
                        }
                    }
                }

                return true;
            } else {
                return target != null && tryPlaceContainedLiquid(bucket, player, worldIn, target.getBlockPos().offset(target.sideHit), null);
            }
        }
    }

    @Override
    public String getDescription() {
        return "Buckets no longer can place water sources, making them a commodity";
    }


    public static class BucketHandlerWrapper implements IBucketPickupHandler {
        public Block block;

        public BucketHandlerWrapper(Block block) {
            this.block = block;
        }

        @Override
        public Fluid pickupFluid(IWorld worldIn, BlockPos pos, IBlockState state) {
            if (block instanceof BlockFlowingFluid) {
                Fluid fluid = (Fluid) ReflectionHelper.getValue(BlockFlowingFluid.class, block, "fluid");
                return fluid;
            }
            return null;
        }
    }
}
