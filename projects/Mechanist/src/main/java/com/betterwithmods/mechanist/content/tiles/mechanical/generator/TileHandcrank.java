/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles.mechanical.generator;

import com.betterwithmods.core.Tags;
import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.api.mech.IMechanicalPower;
import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.TileGenerator;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockHandcrank;
import com.google.common.collect.Lists;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileHandcrank extends TileGenerator {

    public TileHandcrank() {
        super(Registrars.Tiles.TILE_HANDCRANK);
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getInputs() {
        return Lists.newArrayList();
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getOutputs() {
        return FacingUtils.excluding(EnumFacing.UP);
    }

    @Override
    public void updateBlock() {
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        IBlockState state = world.getBlockState(pos.offset(facing));
        if (state.isIn(Tags.Blocks.HANDCRANKABLE)) {
            return power;
        }
        return null;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull EnumFacing facing) {
        return null;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        return world.destroyBlock(pos, true);
    }

    @Override
    public void calculatePowerSource() {
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BlockHandcrank) {
            if (state.get(BlockHandcrank.PROGRESS) > 0) {
                this.powerSource = new Power(1, 1);
                //Handcrank can only powerSource to one machine
                int count = 0;
                for (EnumFacing facing : getOutputs()) {
                    LazyOptional<IMechanicalPower> mech = BWMApi.MECHANICAL_UTILS.getMechanicalPower(world, pos.offset(facing), facing.getOpposite());
                    if (mech.isPresent())
                        count++;
                }

                if (count > 1) {
                    overpower(world, pos);
                    return;
                }

                if (cooldown > cooldownTicks) {
                    EnvironmentUtils.playSound(world, pos, SoundEvents.BLOCK_WOODEN_BUTTON_CLICK_ON, SoundCategory.BLOCKS, 1.0F, 2.0F);
                    world.setBlockState(pos, state.cycle(BlockHandcrank.PROGRESS));
                    cooldown = 0;
                }
                cooldown++;
            } else {
                this.powerSource = new Power(0, 0);
            }
        }
    }


    private int cooldown = 0;
    private static final int cooldownTicks = 8;

    private static final String COOLDOWN = "cooldown";

    @Nonnull
    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        compound.setInt(COOLDOWN, cooldown);
        return super.write(compound);
    }

    @Override
    public void read(NBTTagCompound compound) {
        if (compound.hasKey(COOLDOWN)) {
            cooldown = compound.getInt(COOLDOWN);
        }
        super.read(compound);
    }

}
