/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.dynamic.mini;

import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.content.blocks.dynamic.BlockDynWaterRotate;
import com.betterwithmods.mechanist.content.blocks.dynamic.PlacementUtils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockMoulding extends BlockDynWaterRotate {

    private static final EnumProperty<EnumMoulding> ORIENTATION = EnumProperty.create("orientation", EnumMoulding.class);

    public BlockMoulding(Properties properties) {
        super(properties);
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(ORIENTATION);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState()
                .with(WATERLOGGED, shouldWaterlog(context))
                .with(ORIENTATION, EnumMoulding.getPlacement(context));
    }

    @Override
    public void onRotate(World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.cycle(ORIENTATION));
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.get(ORIENTATION).getShape();
    }

    private enum EnumMoulding implements IStringSerializable {

        NORTH_UP("north_up", Block.makeCuboidShape(0.0D, 8.0D, 0.0D, 16.0, 16.0, 8.0D)),
        SOUTH_UP("south_up", Block.makeCuboidShape(8.0D, 8.0D, 0.0D, 16.0, 16.0, 16.0)),
        WEST_UP("west_up", Block.makeCuboidShape(0.0D, 8.0D, 0.0D, 8.0D, 16.0, 16.0)),
        EAST_UP("east_up", Block.makeCuboidShape(0.0D, 8.0D, 8.0D, 16.0, 16.0, 16.0)),
        NORTH_DOWN("north_down", Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0, 8.0D, 8.0D)),
        SOUTH_DOWN("south_down", Block.makeCuboidShape(8.0D, 0.0D, 0.0D, 16.0, 8.0D, 16.0)),
        WEST_DOWN("west_down", Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 8.0D, 8.0D, 16.0)),
        EAST_DOWN("east_down", Block.makeCuboidShape(0.0D, 0.0D, 8.0D, 16.0, 8.0D, 16.0)),
        SOUTH_WEST("south_west", Block.makeCuboidShape(0.0D, 0.0D, 8.0D, 8.0D, 16.0, 16.0)),
        NORTH_WEST("north_west", Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 8.0D, 16.0, 8.0D)),
        NORTH_EAST("north_east", Block.makeCuboidShape(8.0D, 0.0D, 0.0D, 16.0, 16.0, 8.0D)),
        SOUTH_EAST("south_east", Block.makeCuboidShape(8.0D, 0.0D, 8.0D, 16.0, 16.0, 16.0));

        private final String name;
        private final VoxelShape shape;

        EnumMoulding(String name, VoxelShape shape) {
            this.name = name;
            this.shape = shape;
        }

        @Override
        public String getName() {
            return name;
        }


        public VoxelShape getShape() {
            return shape;
        }

        public static EnumMoulding getPlacement(BlockItemUseContext context) {

            Vec3d hit = WorldUtils.fromContext(context);
            EnumFacing facing = context.getFace();
            double hitXFromCenter = hit.x - PlacementUtils.CENTER_OFFSET;
            double hitYFromCenter = hit.y - PlacementUtils.CENTER_OFFSET;
            double hitZFromCenter = hit.z - PlacementUtils.CENTER_OFFSET;

            switch (facing.getAxis()) {
                case Y:
                    int corner = PlacementUtils.getCorner(hitXFromCenter, hitZFromCenter);
                    if (corner != -1) {
                        EnumMoulding[] corners = new EnumMoulding[]{SOUTH_EAST, NORTH_EAST, NORTH_WEST, SOUTH_WEST};
                        return corners[corner];
                    } else if (hitYFromCenter > 0) {
                        if (PlacementUtils.isMax(hitXFromCenter, hitZFromCenter)) {
                            return hitXFromCenter > 0 ? SOUTH_DOWN : WEST_DOWN;
                        } else {
                            return hitZFromCenter > 0 ? EAST_DOWN : NORTH_DOWN;
                        }
                    } else {
                        if (PlacementUtils.isMax(hitXFromCenter, hitZFromCenter)) {
                            return hitXFromCenter > 0 ? SOUTH_UP : WEST_UP;
                        } else {
                            return hitZFromCenter > 0 ? EAST_UP : NORTH_UP;
                        }
                    }
                case X:
                    corner = PlacementUtils.getCorner(hitYFromCenter, hitZFromCenter);
                    if (corner != -1) {
                        EnumMoulding[] corners = new EnumMoulding[]{EAST_UP, NORTH_UP, NORTH_DOWN, EAST_DOWN};
                        return corners[corner];
                    } else if (hitXFromCenter > 0) {
                        if (PlacementUtils.isMax(hitYFromCenter, hitZFromCenter)) {
                            return hitYFromCenter > 0 ? WEST_UP : WEST_DOWN;
                        } else {
                            return hitYFromCenter > 0 ? SOUTH_WEST : NORTH_WEST;
                        }
                    } else {
                        if (PlacementUtils.isMax(hitYFromCenter, hitZFromCenter)) {
                            return hitYFromCenter > 0 ? SOUTH_UP : SOUTH_DOWN;
                        } else {
                            return hitZFromCenter > 0 ? SOUTH_EAST : NORTH_EAST;
                        }
                    }
                case Z:
                    corner = PlacementUtils.getCorner(hitYFromCenter, hitXFromCenter);
                    if (corner != -1) {
                        EnumMoulding[] corners = new EnumMoulding[]{SOUTH_UP, WEST_UP, WEST_DOWN, SOUTH_DOWN};
                        return corners[corner];
                    } else if (hitZFromCenter > 0) {
                        if (PlacementUtils.isMax(hitXFromCenter, hitYFromCenter)) {
                            return hitXFromCenter > 0 ? NORTH_EAST : NORTH_WEST;
                        } else {
                            return hitYFromCenter > 0 ? NORTH_UP : NORTH_DOWN;
                        }
                    } else {
                        if (PlacementUtils.isMax(hitXFromCenter, hitYFromCenter)) {
                            return hitXFromCenter > 0 ? SOUTH_EAST : SOUTH_WEST;
                        } else {
                            return hitXFromCenter > 0 ? EAST_UP : EAST_DOWN;
                        }
                    }
                default:
                    return EnumMoulding.NORTH_DOWN;
            }
        }


    }

}
