/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.cooking;

import com.betterwithmods.core.base.game.BlockStateProperties;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.VoxelUtils;
import com.betterwithmods.mechanist.client.gui.PotScreen;
import com.betterwithmods.mechanist.content.container.PotContainer;
import com.betterwithmods.mechanist.content.tiles.cooking.TilePot;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nullable;
import java.util.Map;

public class BlockPot extends BlockBase {
    private static final DirectionProperty FACING = BlockStateProperties.FACING_EXCEPT_DOWN;

    public BlockPot(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(FACING, EnumFacing.UP));
        setRenderLayer(BlockRenderLayer.CUTOUT);
        setIsFullCube((s) -> false);
    }

    private IBlockState withFacing(IBlockState state, EnumFacing facing) {
        if (facing == EnumFacing.DOWN)
            facing = EnumFacing.UP;
        return state.with(FACING, facing);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(FACING);
    }

    public void tilt(World worldIn, BlockPos pos, EnumFacing facing) {
        IBlockState state = worldIn.getBlockState(pos);
        worldIn.setBlockState(pos, withFacing(state, facing));
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        EnumFacing facing = state.get(FACING);
        return SHAPES.getOrDefault(facing, VoxelShapes.fullCube());
    }

    private static final Map<EnumFacing, VoxelShape> SHAPES = Maps.newEnumMap(EnumFacing.class);

    static {
        {
            //UP
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 2.0D, 3.0D, 13.0D, 16.0D, 13.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 2, 1.0D, 15.0D, 16.0D, 15.0D);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 2, 0, 16, 14, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2.0D, 0, 2.0D, 14.0D, 2, 14.0D);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(EnumFacing.UP, WALLS);
        }
        {
            //SOUTH
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 3.0, 2.0D, 13.0D, 13.0, 16.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 1, 2, 15.0D, 15D, 16D);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 0, 2, 16, 16, 14);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2, 2, 0, 14.0D, 14, 2);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(EnumFacing.SOUTH, WALLS);
        }
        {
            //NORTH
            VoxelShape INSIDE = Block.makeCuboidShape(3.0D, 3.0, 0, 13.0D, 13.0, 14.0D);
            VoxelShape WALLS_A = Block.makeCuboidShape(1.0D, 1, 0, 15.0D, 15D, 14);
            VoxelShape WALLS_B = Block.makeCuboidShape(0, 0, 2, 16, 16, 14);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(2, 2, 14, 14.0D, 14, 16);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(EnumFacing.NORTH, WALLS);
        }
        {
            //EAST
            VoxelShape INSIDE = Block.makeCuboidShape(2, 3.0, 3.0D, 16, 13.0, 13);
            VoxelShape WALLS_A = Block.makeCuboidShape(2, 1, 1, 16, 15D, 15);
            VoxelShape WALLS_B = Block.makeCuboidShape(2, 0, 0, 14, 16, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(0, 2, 2, 2, 14, 14);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(EnumFacing.EAST, WALLS);
        }
        {
            //WEST
            VoxelShape INSIDE = Block.makeCuboidShape(0, 3.0, 3.0D, 14, 13.0, 13);
            VoxelShape WALLS_A = Block.makeCuboidShape(0, 1, 1, 14, 15D, 15);
            VoxelShape WALLS_B = Block.makeCuboidShape(2, 0, 0, 14, 16, 16);
            VoxelShape WALLS_BOTTOM = Block.makeCuboidShape(14, 2, 2, 16, 14, 14);
            VoxelShape WALLS = VoxelShapes.combineAndSimplify(VoxelUtils.or(WALLS_A, WALLS_B, WALLS_BOTTOM), INSIDE, IBooleanFunction.ONLY_FIRST);
            SHAPES.put(EnumFacing.WEST, WALLS);
        }
    }

    @Override
    public void onEntityCollision(IBlockState state, World worldIn, BlockPos pos, Entity entityIn) {
        super.onEntityCollision(state, worldIn, pos, entityIn);
    }

    @Override
    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote()) {
            NetworkHooks.openGui((EntityPlayerMP) player, getInteractionObject(worldIn, pos), pos);
        }
        return true;
    }
    @OnlyIn(Dist.CLIENT)
    @Nullable
    @Override
    public GuiScreen getScreen(World world, BlockPos pos, EntityPlayer player, IInteractionObject interactionObject) {
        if(interactionObject instanceof TilePot) {
            TilePot cauldron = (TilePot) interactionObject;
            return new PotScreen((PotContainer) interactionObject.createContainer(player.inventory, player), cauldron);
        }
        return null;
    }
}

