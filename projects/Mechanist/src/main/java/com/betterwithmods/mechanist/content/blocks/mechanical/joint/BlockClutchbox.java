/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.mechanical.joint;

import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.content.tiles.mechanical.joint.TileClutchbox;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockClutchbox extends BlockPowerJoint {

    public static final BooleanProperty LIT = BlockStateProperties.LIT;

    public BlockClutchbox(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(LIT, false));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(LIT);
    }

    private boolean isCurrentlyValid(World worldIn, BlockPos pos) {
        boolean lit = isLit(worldIn, pos);
        boolean powered = WorldUtils.isPowered(worldIn, pos);
        return lit == powered;
    }

    private boolean isLit(IBlockReader worldIn, BlockPos pos) {
        IBlockState state = worldIn.getBlockState(pos);
        if(state.getBlock() == this) {
            return state.get(LIT);
        }
        return false;
    }

    @Override
    public void onChanged(World world, BlockPos pos, IBlockState state) {
        if (!world.isRemote) {
            if (!isCurrentlyValid(world, pos)) {
                boolean lit = isLit(world, pos);
                world.setBlockState(pos, state.with(LIT, !lit), 2);
            }
        }
        super.onChanged(world, pos, state);
    }

    public int tickRate(IWorldReaderBase worldIn) {
        return 4;
    }


    @Override
    public boolean canConnectRedstone(IBlockState state, IBlockReader world, BlockPos pos, @Nullable EnumFacing side) {
        return side != state.get(FACING);
    }


    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(IBlockState state, IBlockReader world) {
        return new TileClutchbox();
    }

}
