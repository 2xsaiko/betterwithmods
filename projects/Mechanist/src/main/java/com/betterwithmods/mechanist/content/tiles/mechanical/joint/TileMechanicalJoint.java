/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles.mechanical.joint;

import com.betterwithmods.core.api.mech.IPower;
import com.betterwithmods.core.base.game.tile.TileMechanicalPower;
import com.betterwithmods.core.impl.Power;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.core.utilties.VectorBuilder;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockMechanicalJoint;
import com.betterwithmods.mechanist.content.blocks.mechanical.joint.BlockPowerJoint;
import com.betterwithmods.mechanist.modules.MechanicalPower;
import com.google.common.collect.Lists;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Particles;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.particles.BlockParticleData;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class TileMechanicalJoint extends TileMechanicalPower implements ITickable {

    public TileMechanicalJoint(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public static final Power JOINT = new Power(1, 4);
    public static final Power MAX_POWER = new Power(2, 0);

    public static final String OVERPOWER_TIME = "overpower_time";

    public int overpowerTime = -1;

    public EnumFacing getInput() {
        IBlockState state = world.getBlockState(pos);
        return state.get(BlockMechanicalJoint.FACING);
    }


    @Nonnull
    @Override
    public Iterable<EnumFacing> getInputs() {
        return Lists.newArrayList(getInput());
    }

    @Nonnull
    @Override
    public Iterable<EnumFacing> getOutputs() {
        return FacingUtils.excluding(getInput());
    }

    @Override
    public void updateBlock() {
        IBlockState state = world.getBlockState(pos);
        if (state.getBlock() instanceof BlockPowerJoint) {
            world.setBlockState(pos, state.with(BlockPowerJoint.POWERED, power.getTorque() > 0));
        }
    }

    @Nullable
    @Override
    public IPower getOutput(@Nonnull IWorld world, @Nonnull BlockPos pos, @Nonnull EnumFacing facing) {
        if (facing != getInput()) {
            if (power.getTorque() > 0)
                return JOINT;
        }
        return null;
    }

    @Nullable
    @Override
    public IPower getMaximumInput(@Nonnull EnumFacing facing) {
        return MAX_POWER;
    }

    @Nullable
    @Override
    public IPower getMinimumInput() {
        return null;
    }

    @Override
    public boolean overpower(World world, BlockPos pos) {
        overpowerTime = 0;
        markDirty();
        return false;
    }

    protected abstract void breakBlock(World world, BlockPos pos);

    private static final int PARTICLE_EVENT = 100;


    @Override
    public void tick() {
        if (world.isRemote) {
            return;
        }

        if (overpowerTime < 0) {
            return;
        }

        if (overpowerTime >= MechanicalPower.GEARBOX_OVERPOWER_DELAY.get()) {
            EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ZOMBIE_BREAK_WOODEN_DOOR, SoundCategory.BLOCKS, 1, 1);
            breakBlock(world, pos);
            overpowerTime = 0;
            return;
        }

        IPower max = getMaximumInput(getInput());
        if (max != null && power.getTorque() > max.getTorque()) {
            world.addBlockEvent(pos, getBlockState().getBlock(), PARTICLE_EVENT, (int) ((double)overpowerTime / MechanicalPower.GEARBOX_OVERPOWER_DELAY.get() * 100));
            EnvironmentUtils.playRandomSound(world, pos, SoundEvents.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR, SoundCategory.BLOCKS, 1, 1, 0.1);
            overpowerTime++;
        } else {
            overpowerTime = -1;
        }
        markDirty();
    }

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if (id == PARTICLE_EVENT) {
            EnvironmentUtils.forEachParticle(world, pos, Particles.LARGE_SMOKE, 6, EnvironmentUtils.RANDOM_PARTICLE, VectorBuilder.ZERO);
            EnvironmentUtils.forEachParticleRandom(world, pos, new BlockParticleData(Particles.BLOCK, getBlockState()), type / 15, EnvironmentUtils.RANDOM_PARTICLE, VectorBuilder.ZERO, 0.2);
            return true;
        }
        return false;
    }


    @Nonnull
    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        compound.setInt(OVERPOWER_TIME, overpowerTime);
        return super.write(compound);
    }

    @Override
    public void read(NBTTagCompound compound) {
        if (compound.hasKey(OVERPOWER_TIME)) {
            overpowerTime = compound.getInt(OVERPOWER_TIME);
        }
        super.read(compound);
    }
}
