/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.tiles;

import com.betterwithmods.core.base.game.tile.TileBase;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.client.baking.CamoFactory;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelDataMap;

import javax.annotation.Nonnull;

public class TileCamoflage extends TileBase {

    private IBlockState parentState;

    public TileCamoflage(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public TileCamoflage() {
        super(Registrars.Tiles.TILE_CAMOFLAGE);
    }

    @Nonnull
    @Override
    public IModelData getModelData() {
        ModelDataMap.Builder builder = new ModelDataMap.Builder();
        builder.withInitial(CamoFactory.PARENT_STATE, getParentState());
        return builder.build();
    }

    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        super.write(compound);
        if (parentState != null) {
            compound.setTag("parent", NBTUtil.writeBlockState(parentState));
        }
        return compound;
    }

    @Override
    public void read(NBTTagCompound compound) {
        super.read(compound);
        if (compound.contains("parent", 10)) {
            NBTTagCompound parentTag = (NBTTagCompound) compound.getTag("parent");
            setParentState(NBTUtil.readBlockState(parentTag));
        }
    }

    public IBlockState getParentState() {
        return parentState;
    }

    public void setParentState(IBlockState parentState) {
        this.parentState = parentState;
        this.update();
        this.updateClient();
    }

    public ItemStack getItemStack(IBlockState state) {
        return fromParent(state.getBlock(), getParentState());
    }

    public static ItemStack fromParent(IItemProvider item, IBlockState parent) {
        return fromParent(item, parent, 1);
    }

    public static ItemStack fromParent(IItemProvider item, IBlockState parent, int count) {
        if (parent != null) {
            ItemStack stack = new ItemStack(item, count);
            stack.getOrCreateChildTag("BlockEntityTag")
                    .setTag("parent", NBTUtil.writeBlockState(parent));
            return stack;
        }
        return ItemStack.EMPTY;
    }

    public void loadFromItemStack(ItemStack stack) {
        NBTTagCompound tag = stack.getChildTag("BlockEntityTag");
        if (tag != null) {
            if (tag.hasKey("parent")) {
                NBTTagCompound parentTag = (NBTTagCompound) tag.getTag("parent");
                setParentState(NBTUtil.readBlockState(parentTag));
            }
        }
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
//        Mechanist.LOGGER.info("Packet compound {}, {}", pkt.getPos(), pkt.getNbtCompound());
        super.onDataPacket(net, pkt);
    }
}
