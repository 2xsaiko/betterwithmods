/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.mechanical.joint;

import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.config.CoreCommon;
import com.betterwithmods.core.utilties.MechanicalUtils;
import com.betterwithmods.mechanist.content.blocks.mechanical.BlockAxleBase;
import com.betterwithmods.mechanist.content.handlers.BlockRotationHandler;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class BlockMechanicalJoint extends BlockBase {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;
    private static final BooleanProperty UP = BlockStateProperties.UP;
    private static final BooleanProperty DOWN = BlockStateProperties.DOWN;
    private static final BooleanProperty NORTH = BlockStateProperties.NORTH;
    private static final BooleanProperty EAST = BlockStateProperties.EAST;
    private static final BooleanProperty SOUTH = BlockStateProperties.SOUTH;
    private static final BooleanProperty WEST = BlockStateProperties.WEST;


    public BlockMechanicalJoint(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState()
                .with(FACING, EnumFacing.UP)
                .with(UP, false)
                .with(DOWN, false)
                .with(NORTH, false)
                .with(EAST, false)
                .with(SOUTH, false)
                .with(WEST, false)
        );
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(FACING, UP, DOWN, NORTH, EAST, SOUTH, WEST);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        IWorld world = context.getWorld();
        BlockPos pos = context.getPos();
        IBlockState stateIn = getDefaultState().with(FACING, context.getFace());
        return getConnections(stateIn, world, pos);
    }


    @Override
    public IBlockState updatePostPlacement(IBlockState stateIn, EnumFacing facing, IBlockState facingState, IWorld world, BlockPos pos, BlockPos facingPos) {
        return getConnections(stateIn, world, pos);
    }

    public IBlockState getConnections(IBlockState stateIn, IWorld world, BlockPos pos) {
        return stateIn
                .with(UP, isProperAxle(stateIn, world, pos, EnumFacing.UP))
                .with(DOWN, isProperAxle(stateIn, world, pos, EnumFacing.DOWN))
                .with(NORTH, isProperAxle(stateIn, world, pos, EnumFacing.NORTH))
                .with(EAST, isProperAxle(stateIn, world, pos, EnumFacing.EAST))
                .with(SOUTH, isProperAxle(stateIn, world, pos, EnumFacing.SOUTH))
                .with(WEST, isProperAxle(stateIn, world, pos, EnumFacing.WEST));
    }


    private boolean isProperAxle(IBlockState state, IWorld world, BlockPos pos, EnumFacing facing) {

        //Don't put an output on the input face
        if (state.getBlock() == this && state.get(FACING) == facing)
            return false;

        EnumFacing.Axis axis = facing.getAxis();
        IBlockState offsetState = world.getBlockState(pos.offset(facing));
        Block block = offsetState.getBlock();
        if (block instanceof BlockAxleBase) {
            EnumFacing.Axis axleAxis = ((BlockAxleBase) block).getAxis(offsetState);
            return axis == axleAxis;
        }
        return false;
    }


    @Override
    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if(CoreCommon.DEBUGGING.get()) {
            MechanicalUtils.debug(worldIn,pos, player,hand);
        }
        return BlockRotationHandler.cycleRotation(player,worldIn,pos,state, this::onRotate);
    }


    public void onRotate(World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.cycle(FACING));
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }


}
