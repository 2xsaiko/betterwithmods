/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist;

import com.betterwithmods.core.References;
import com.betterwithmods.core.api.BWMApi;
import com.betterwithmods.core.base.setup.ModBase;
import com.betterwithmods.core.base.setup.ModConfiguration;
import com.betterwithmods.core.base.setup.proxy.BaseClientProxy;
import com.betterwithmods.core.base.setup.proxy.BaseServerProxy;
import com.betterwithmods.core.base.setup.proxy.IProxy;
import com.betterwithmods.core.utilties.MechanicalUtils;
import com.betterwithmods.mechanist.client.entities.RenderDynamite;
import com.betterwithmods.mechanist.client.entities.RenderMiningCharge;
import com.betterwithmods.mechanist.config.Client;
import com.betterwithmods.mechanist.config.Common;
import com.betterwithmods.mechanist.content.entities.EntityDynamite;
import com.betterwithmods.mechanist.content.entities.EntityMiningCharge;
import com.betterwithmods.mechanist.modules.*;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import org.apache.logging.log4j.Logger;

@Mod(References.MODID_MECHANIST)
public class Mechanist extends ModBase {

    public static Logger LOGGER;

    public static ModConfiguration<Client, Common> configs;

    public static IProxy PROXY = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);

    static {
        BWMApi.MECHANICAL_UTILS = new MechanicalUtils();
    }

    public Mechanist() {
        super(References.MODID_MECHANIST, References.NAME_MECHANIST);
        LOGGER = this.getLogger();
        new Registrars(References.MODID_MECHANIST, getLogger());
        addProxy(PROXY);
        addModule(new MechanicalPower());
        addModule(new RedstoneLamp());
        addModule(new DropHempSeeds());
        addModule(new DynamicBlocks());
        addModule(new BarkStripping());
        addModule(new LogShattering());
        addModule(new StrategicWater());
        addModule(new LumberChopping());
        configs = new ModConfiguration<>(Client::new, Common::new);
    }

    @Override
    public void onClientSetup(FMLClientSetupEvent event) {
        super.onClientSetup(event);
        RenderingRegistry.registerEntityRenderingHandler(EntityMiningCharge.class, RenderMiningCharge::new);
        RenderingRegistry.registerEntityRenderingHandler(EntityDynamite.class, RenderDynamite::new);
    }

    @Override
    public void onCommonSetup(FMLCommonSetupEvent event) {
        super.onCommonSetup(event);
    }


    private static class ServerProxy extends BaseServerProxy { }

    private static class ClientProxy extends BaseClientProxy {

        public ClientProxy() {
            MinecraftForge.EVENT_BUS.register(this);
        }

        @SubscribeEvent
        public void registerModels(ModelRegistryEvent event) {
            //TODO axle animation
//            ClientRegistry.bindTileEntitySpecialRenderer(TileAxle.class, new TileEntityRendererAnimation<>());
        }
    }
}
