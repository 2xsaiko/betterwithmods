/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks;

import com.betterwithmods.core.base.game.block.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.fluid.IFluidState;
import net.minecraft.init.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class BlockVineTrap extends BlockBase {

    private static final BooleanProperty TYPE = BooleanProperty.create("type");
    private static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    private VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 0, 0, 16, 2, 16),
            Block.makeCuboidShape(0, 14, 0, 16, 16, 16)
    };

    public BlockVineTrap(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(TYPE, false));
        setRenderLayer(BlockRenderLayer.CUTOUT);
        setIsFullCube(state -> false);
    }


    @SuppressWarnings("deprecation")
    @Override
    public VoxelShape getCollisionShape(@Nonnull IBlockState  state,@Nonnull IBlockReader world,@Nonnull BlockPos pos) {
        return VoxelShapes.empty();
    }


    @SuppressWarnings("deprecation")
    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader world, BlockPos pos) {
        boolean type = state.get(TYPE);
        return type ? SHAPES[1] : SHAPES[0];
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(TYPE).add(WATERLOGGED);
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        IFluidState ifluidstate = context.getWorld().getFluidState(context.getPos());
        IBlockState iblockstate1 = this.getDefaultState().with(TYPE, false).with(WATERLOGGED, ifluidstate.getFluid() == Fluids.WATER);
        EnumFacing enumfacing = context.getFace();
        return enumfacing != EnumFacing.DOWN && (enumfacing == EnumFacing.UP || !((double)context.getHitY() > 0.5D)) ? iblockstate1 : iblockstate1.with(TYPE, true);
    }

    @Override
    public boolean propagatesSkylightDown(IBlockState p_200123_1_, @Nonnull IBlockReader p_200123_2_, @Nonnull BlockPos p_200123_3_) {
        return true;
    }

    @SuppressWarnings("deprecation")
    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean isSideInvisible(IBlockState state, IBlockState adjacentBlockState, EnumFacing side) {
        if(state.getBlock() == adjacentBlockState.getBlock()) {
            return state.get(TYPE) == state.get(TYPE);
        }
        return false;
    }
}
