/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.entities;

import com.betterwithmods.core.utilties.ItemUtils;
import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.Registrars;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.MoverType;
import net.minecraft.init.Particles;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class EntityDynamite extends Entity implements IProjectile {
    private static final byte FUSE_UPDATE_TAG = 100;
    private static final DataParameter<Integer> FUSE;
    private int fuse;

    private EntityLivingBase thrower;

    public EntityDynamite(World world) {
        super(Registrars.Entities.DYNAMITE, world);
        this.setSize(0.25F, 0.4F);
        this.setPosition(0,0,0);
        this.fuse = 0;
        this.preventEntitySpawning = true;
        this.isImmuneToFire = true;
    }

    public EntityDynamite(World world, EntityLivingBase thrower, int fuse) {
        super(Registrars.Entities.DYNAMITE, world);
        this.thrower = thrower;
        this.fuse = fuse;
        this.setSize(0.25F, 0.25F);
        this.setLocationAndAngles(thrower.posX, thrower.posY + thrower.getEyeHeight(), thrower.posZ, thrower.rotationYaw, thrower.rotationPitch);
        this.posX -= MathHelper.cos(this.rotationYaw / 180.0F * pi) * 0.16F;
        this.posY -= 0.1D;
        this.posZ -= MathHelper.sin(this.rotationYaw / 180.0F * pi) * 0.16F;
        this.setPosition(this.posX, this.posY, this.posZ);
        this.motionX = (-MathHelper.sin(this.rotationYaw / 180.0F * pi) * MathHelper.cos(this.rotationPitch / 180.0F * pi) * 0.4F);
        this.motionZ = (MathHelper.cos(this.rotationYaw / 180.0F * pi) * MathHelper.cos(this.rotationPitch / 180.0F * pi) * 0.4F);
        this.motionY = (-MathHelper.sin(this.rotationPitch / 180.0F * pi) * 0.4F);
        this.shoot(this.motionX, this.motionY, this.motionZ, 0.75F, 1.0F);
    }

    protected void registerData() {
        this.dataManager.register(FUSE, -1);
    }

    @Override
    public void handleStatusUpdate(byte tag) {
        if (tag == FUSE_UPDATE_TAG) {
            this.fuse = 100;
        }
    }

    public void explode() {
        float intensity = 1.5F;
        world.createExplosion(null, this.posX, this.posY, this.posZ, intensity, true);
        //TODO redneck fishing
    }

    @Override
    public void tick() {

        //Convert back to an item if unlit
        if(getFuse() <= -1) {
            if(!world.isRemote) {
                if(onGround) {
                    ItemUtils.ejectStackOffset(world, WorldUtils.fromEntity(this), new ItemStack(Registrars.Items.DYNAMITE));
                    remove();
                }
            }
        }

        if (getFuse() > 0) {
            if (!world.isRemote) {
                //Set fuse value on client for particles
                world.setEntityState(this, FUSE_UPDATE_TAG);
                //Play sounds every second
                if (getFuse() % 20 == 0) {
                    world.playSound(null, getPosition(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.NEUTRAL, 1.0F, 1.0F);
                }
            }

            float smokeOffset = 0.25F;
            world.spawnParticle(Particles.SMOKE, this.posX - this.motionX * smokeOffset, this.posY - this.motionY * smokeOffset, this.posZ - this.motionZ * smokeOffset, this.motionX, this.motionY, this.motionZ);

            if (getFuse() <= 1) {
                if (!this.getEntityWorld().isRemote) {
                    explode();
                }
                this.remove();
            }
            fuse--;
        }


        this.prevPosX = this.posX;
        this.prevPosY = this.posY;
        this.prevPosZ = this.posZ;
        this.motionY -= 0.04D;
        this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
        this.motionX *= 0.98D;
        this.motionY *= 0.98D;
        this.motionZ *= 0.98D;

        if (this.onGround) {
            this.motionX *= 0.7D;
            this.motionZ *= 0.7D;
            this.motionY *= -0.5D;
        }
    }

    @Override
    protected void readAdditional(NBTTagCompound compound) {
    }

    @Override
    protected void writeAdditional(NBTTagCompound compound) {
    }

    public void setFuse(int fuse) {
        this.dataManager.set(FUSE, fuse);
        this.fuse = fuse;
    }

    public int getFuse() {
        return this.fuse;
    }

    public void notifyDataManagerChange(DataParameter<?> parameter) {
        if (FUSE.equals(parameter)) {
            this.fuse = this.dataManager.get(FUSE);
        }
    }

    static {
        FUSE = EntityDataManager.createKey(EntityDynamite.class, DataSerializers.VARINT);
    }

    @Override
    public void shoot(double dX, double dY, double dZ, float angle, float f) {
        float sqrt = MathHelper.sqrt(dX * dX + dY * dY + dZ * dZ);
        dX /= sqrt;
        dY /= sqrt;
        dZ /= sqrt;
        dX += this.rand.nextGaussian() * 0.0075D * f;
        dY += this.rand.nextGaussian() * 0.0075D * f;
        dZ += this.rand.nextGaussian() * 0.0075D * f;
        dX *= angle;
        dY *= angle;
        dZ *= angle;
        this.motionX = dX;
        this.motionY = dY;
        this.motionZ = dZ;
        float pitch = MathHelper.sqrt(dX * dX + dZ * dZ);
        this.prevRotationYaw = (this.rotationYaw = (float) (Math.atan2(dX, dZ) * 180.0D / pi));
        this.prevRotationPitch = (this.rotationPitch = (float) (Math.atan2(dY, pitch) * 180.0D / pi));
    }
    private static final float pi = 3.141593F;
}
