/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.dynamic.mini;

import com.betterwithmods.core.utilties.WorldUtils;
import com.betterwithmods.mechanist.content.blocks.dynamic.BlockDynWaterRotate;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import static com.betterwithmods.mechanist.content.blocks.dynamic.PlacementUtils.getCorner;

public class BlockCorner extends BlockDynWaterRotate {

    private static final EnumProperty<EnumCorner> ORIENTATION = EnumProperty.create("orientation", EnumCorner.class);

    public BlockCorner(Properties properties) {
        super(properties);
        setDefaultState(getDefaultState().with(ORIENTATION,EnumCorner.DOWN_NORTH));
        setIsFullCube(state -> false);
        setRenderLayer(BlockRenderLayer.CUTOUT);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(ORIENTATION);
    }

    @Override
    public void onRotate(World worldIn, BlockPos pos, IBlockState state) {
        worldIn.setBlockState(pos, state.cycle(ORIENTATION));
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.get(ORIENTATION).getShape();
    }

    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState()
                .with(WATERLOGGED, shouldWaterlog(context))
                .with(ORIENTATION, EnumCorner.getPlacement(context));
    }

    public enum EnumCorner implements IStringSerializable {
        DOWN_NORTH("down_north", Block.makeCuboidShape(0.0D, 0.0D, 8.0D, 8.0D, 8.0D, 16.0D)),
        DOWN_SOUTH("down_south",Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 8.0D, 8.0D, 8.0D)),
        DOWN_EAST("down_east",Block.makeCuboidShape(8.0D, 0.0D, 8.0D, 16.0D, 8.0D, 16.0D)),
        DOWN_WEST("down_west",Block.makeCuboidShape(8.0D, 0.0D, 0.0D, 16.0D, 8.0D, 8.0D)),
        UP_NORTH("up_north",Block.makeCuboidShape(0.0D, 8.0D, 8.0D, 8.0D, 16.0D, 16.0D)),
        UP_SOUTH("up_south",Block.makeCuboidShape(0.0D, 8.0D, 0.0D, 8.0D, 16.0D, 8.0D)),
        UP_EAST("up_east", Block.makeCuboidShape(8.0D, 8.0D, 8.0D, 16.0D, 16.0D, 16.0D)),
        UP_WEST("up_west", Block.makeCuboidShape(8.0D, 8.0D, 0.0D, 16.0D, 16.0D, 8.0D));

        public static EnumCorner[] VALUES = values();

        private final String name;
        private final VoxelShape shape;



        EnumCorner(String name, VoxelShape shape) {
            this.name = name;
            this.shape = shape;
        }

        @Override
        public String getName() {
            return name;
        }

        public VoxelShape getShape() {
            return shape;
        }


        public static EnumCorner fromFace(EnumFacing facing) {
            if (facing != null)
                return EnumCorner.VALUES[facing.getIndex()];
            return EnumCorner.DOWN_EAST;
        }


        public static EnumCorner getPlacement(BlockItemUseContext context) {
            Vec3d hit = WorldUtils.fromContext(context);
            EnumFacing facing = context.getFace();
            float hitXFromCenter = (float) (hit.x - 0.5F);
            float hitYFromCenter = (float) (hit.y - 0.5F);
            float hitZFromCenter = (float) (hit.z - 0.5F);
            switch (facing.getAxis()) {
                case Y:
                    int corner = getCorner(hitXFromCenter, hitZFromCenter, 0);
                    if (corner != -1) {
                        int[] corners = hitYFromCenter > 0 ? new int[]{2, 3, 1, 0} : new int[]{6, 7, 5, 4};
                        return EnumCorner.VALUES[corners[corner]];
                    }
                case X:
                    corner = getCorner(hitYFromCenter, hitZFromCenter, 0);
                    if (corner != -1) {
                        int[] corners = hitXFromCenter > 0 ? new int[]{4, 5, 1, 0} : new int[]{6, 7, 3, 2};
                        return EnumCorner.VALUES[corners[corner]];
                    }
                case Z:
                    corner = getCorner(hitYFromCenter, hitXFromCenter, 0);
                    if (corner != -1) {
                        int[] corners = hitZFromCenter > 0 ? new int[]{7, 5, 1, 3} : new int[]{6, 4, 0, 2};
                        return EnumCorner.VALUES[corners[corner]];
                    }
                default:
                    return fromFace(facing.getOpposite());
            }

        }
    }
}
