/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.slabs;

import com.betterwithmods.core.base.game.BlockStateProperties;
import com.betterwithmods.core.utilties.BlockUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import com.betterwithmods.core.utilties.WorldUtils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

import java.util.Random;

public class BlockDirtSnowySlab extends BlockDirtSlabBase {


    protected static final VoxelShape SNOW = Block.makeCuboidShape(0.0D, 8.0D, 0.0D, 16.0D, 10.0D, 16.0D);
    public static final VoxelShape BOTH = VoxelShapes.combineAndSimplify(BOTTOM_SHAPE, SNOW, IBooleanFunction.OR);
    public static final BooleanProperty SNOWY = BlockStateProperties.SNOWY;

    public BlockDirtSnowySlab(Properties builder) {
        super(builder);
        setDefaultState(getDefaultState().with(SNOWY, false));
    }


    @Override
    public void tick(IBlockState state, World worldIn, BlockPos pos, Random random) {
        if (!worldIn.isRemote && !state.get(SNOWY)) {
            Chunk chunk = worldIn.getChunk(pos);
            if (worldIn.isRaining() && worldIn.dimension.canDoRainSnowIce(chunk) && worldIn.rand.nextInt(16) == 0) {
                if (WorldUtils.isValidForSnow(worldIn, pos)) {
                    worldIn.setBlockState(pos, state.with(SNOWY, true));
                }
            }
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(SNOWY);
    }

    @Override
    public void getDrops(IBlockState state, NonNullList<ItemStack> drops, World world, BlockPos pos, int fortune) {
        super.getDrops(state, drops, world, pos, fortune);
        if (state.get(SNOWY)) {
            drops.add(new ItemStack(Items.SNOWBALL));
        }
    }

    @Override
    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        ItemStack held = PlayerUtils.getHeld(player, hand);
        if (held.getItem() == Blocks.SNOW.asItem()) {
            if (!state.get(SNOWY) && side == EnumFacing.UP) {
                BlockUtils.playPlaceSound(worldIn, pos, player, Blocks.SNOW);
                worldIn.setBlockState(pos, state.with(SNOWY, true));
                held.shrink(1);
                return true;
            }
        }
        return super.onBlockActivated(state, worldIn, pos, player, hand, side, hitX, hitY, hitZ);
    }


    @Override
    public VoxelShape getCollisionShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return BOTTOM_SHAPE;
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        if (state.get(SNOWY)) {
            return BOTH;
        }
        return BOTTOM_SHAPE;
    }


}
