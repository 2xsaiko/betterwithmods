/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.redstone;

import com.betterwithmods.core.base.game.block.BlockDigitalRedstone;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFire;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public class BlockHibachi extends BlockDigitalRedstone {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public BlockHibachi(Properties properties) {
        super(properties);
        this.setDefaultState(this.getDefaultState().with(FACING, EnumFacing.UP));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    public EnumFacing getFront(IBlockReader worldIn, BlockPos pos) {
        return worldIn.getBlockState(pos).get(FACING);
    }

    public BlockPos getFrontPos(IBlockReader worldIn, BlockPos pos) {
        return pos.offset(getFront(worldIn, pos));
    }

    public void makeFlame(World worldIn, BlockPos pos) {
        BlockPos front = getFrontPos(worldIn, pos);
        if (worldIn.isAirBlock(front)) {
            worldIn.playSound(null, pos, SoundEvents.ITEM_FIRECHARGE_USE, SoundCategory.BLOCKS, 1.0F, (worldIn.rand.nextFloat() - worldIn.rand.nextFloat()) * 0.2F + 1.0F);
            worldIn.setBlockState(front, ((BlockFire) (Blocks.FIRE)).getStateForPlacement(worldIn, pos));
        }
    }

    @Override
    public void onPowered(IBlockState state, World worldIn, BlockPos pos) {
        makeFlame(worldIn, pos);
    }

    @Override
    public void onUnpowered(IBlockState state, World worldIn, BlockPos pos) {
        if (isValidFlame(worldIn, pos)) {
            BlockPos front = getFrontPos(worldIn, pos);
            worldIn.removeBlock(front);
        }
    }

    @Override
    public boolean isFireSource(IBlockState state, IBlockReader world, BlockPos pos, EnumFacing side) {
        return isLit(world, pos);
    }

    public boolean isValidFlame(IBlockReader worldIn, BlockPos pos) {
        IBlockState state = worldIn.getBlockState(getFrontPos(worldIn, pos));
        return state.getBlock() == Blocks.FIRE;
    }

    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        EnumFacing facing = context.getNearestLookingDirection().getOpposite();
        return this.getDefaultState().with(FACING, facing);
    }

    @Override
    protected void onChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        if(worldIn.isRemote)
            return;
        if (isLit(worldIn, pos)) {
            makeFlame(worldIn, pos);
        }
    }

}
