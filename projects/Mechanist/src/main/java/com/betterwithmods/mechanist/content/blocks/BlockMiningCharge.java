/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks;

import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.mechanist.content.entities.EntityMiningCharge;
import net.minecraft.block.Block;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockMiningCharge extends BlockBase {

    private static final BooleanProperty UNSTABLE = BlockStateProperties.field_212646_x;
    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    private static VoxelShape[] FACING_SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 8, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 8, 16),
            Block.makeCuboidShape(0, 0, 8, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 8),
            Block.makeCuboidShape(8, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 8, 16, 16),
    };


    public BlockMiningCharge(Block.Properties builder) {
        super(builder);
        this.setDefaultState(this.getDefaultState().with(UNSTABLE, false).with(FACING, EnumFacing.UP));
        setRenderLayer(BlockRenderLayer.CUTOUT);
        setIsFullCube((state) -> false);
    }


    @Override
    public boolean isValidPosition(IBlockState state, IWorldReaderBase world, BlockPos pos) {
        EnumFacing facing = state.get(FACING).getOpposite();
        BlockPos offset = pos.offset(facing);
        IBlockState surface = world.getBlockState(offset);
        return surface.getBlockFaceShape(world, offset, facing) == BlockFaceShape.SOLID;
    }

    @Override
    public BlockFaceShape getBlockFaceShape(IBlockReader world, IBlockState state, BlockPos pos, EnumFacing facing) {
        return facing == state.get(FACING) ? BlockFaceShape.SOLID : BlockFaceShape.UNDEFINED;
    }

    @Nullable
    @Override
    public IBlockState getStateForPlacement(BlockItemUseContext context) {
        return getDefaultState().with(FACING, context.getFace());
    }

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader world, BlockPos pos) {
        EnumFacing facing = state.get(FACING);
        return FACING_SHAPES[facing.getIndex()];
    }

    public void onBlockAdded(IBlockState state, World worldIn, BlockPos pos, IBlockState oldState) {
        if (oldState.getBlock() != state.getBlock()) {
            if (worldIn.isBlockPowered(pos)) {
                this.explode(worldIn, pos);
                worldIn.removeBlock(pos);
            }
        }
    }

    public void neighborChanged(IBlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos) {
        if (worldIn.isBlockPowered(pos)) {
            this.explode(worldIn, pos);
            worldIn.removeBlock(pos);
        }
    }

    public void dropBlockAsItemWithChance(IBlockState state, World worldIn, BlockPos pos, float chancePerItem, int fortune) {
        if (!state.get(UNSTABLE)) {
            super.dropBlockAsItemWithChance(state, worldIn, pos, chancePerItem, fortune);
        }
    }

    public void onBlockHarvested(World worldIn, BlockPos pos, IBlockState state, EntityPlayer player) {
        if (!worldIn.isRemote() && !player.isCreative() && state.get(UNSTABLE)) {
            this.explode(worldIn, pos);
        }

        super.onBlockHarvested(worldIn, pos, state, player);
    }

    public void onExplosionDestroy(World worldIn, BlockPos pos, Explosion explosionIn) {
        explode(worldIn, pos, explosionIn.getExplosivePlacedBy());
    }

    public void explode(World world, BlockPos p_196534_2_) {
        this.explode(world, p_196534_2_, null);
    }

    private void explode(World world, BlockPos pos, @Nullable EntityLivingBase exploder) {
        if (!world.isRemote) {
            IBlockState state = world.getBlockState(pos);
            EnumFacing facing = EnumFacing.UP;
            if (state.getBlock() == this)
                facing = state.get(FACING);

            EntityMiningCharge entity = new EntityMiningCharge(world, facing, new Vec3d(pos).add(0.5,0,0.5), exploder);
            world.spawnEntity(entity);
            world.playSound(null, entity.posX, entity.posY, entity.posZ, SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
        }
    }


    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        ItemStack itemstack = player.getHeldItem(hand);
        Item item = itemstack.getItem();
        if (ConstantIngredients.FIRESTARTER.test(itemstack)) {
            this.explode(worldIn, pos, player);
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 11);
            if (item.isDamageable()) {
                itemstack.damageItem(1, player);
            } else {
                itemstack.shrink(1);
            }
            return true;
        }
        return false;
    }

    public void onEntityCollision(IBlockState state, World worldIn, BlockPos pos, Entity entityIn) {
        if (!worldIn.isRemote && entityIn instanceof EntityArrow) {
            EntityArrow entityarrow = (EntityArrow) entityIn;
            Entity entity = entityarrow.func_212360_k();
            if (entityarrow.isBurning()) {
                this.explode(worldIn, pos, entity instanceof EntityLivingBase ? (EntityLivingBase) entity : null);
                worldIn.removeBlock(pos);
            }
        }
    }


    public boolean canDropFromExplosion(Explosion explosionIn) {
        return false;
    }

    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        builder.add(UNSTABLE).add(FACING);
    }
}
