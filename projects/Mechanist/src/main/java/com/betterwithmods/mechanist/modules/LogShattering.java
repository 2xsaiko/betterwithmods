/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.modules;

import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.base.game.ConstantIngredients;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.PlayerUtils;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LogShattering extends ModuleBase {

    private ForgeConfigSpec.IntValue BARK_COUNT, DUST_COUNT, PLANK_COUNT;
    private ForgeConfigSpec.BooleanValue SPECIAL_SOUND;

    public LogShattering() {
        super("log_shattering");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        PLANK_COUNT = builder.comment("Number of Planks dropped when logs are shattered")
                .translation("config.betterwithmods_mechanist.plank_count")
                .defineInRange("plank_count", 2, 0, 64);

        BARK_COUNT = builder.comment("Number of Bark dropped when logs are shattered")
                .translation("config.betterwithmods_mechanist.bark_count")
                .defineInRange("bark_count", 1, 0, 64);

        DUST_COUNT = builder.comment("Number of Saw Dust dropped when logs are shattered")
                .translation("config.betterwithmods_mechanist.dust_count")
                .defineInRange("dust_count", 1, 0, 64);

        SPECIAL_SOUND = builder.comment("Play breaking sounds for shattering logs")
                .translation("config.betterwithmods_mechanist.special_sound")
                .define("special_sound", true);
    }

    @SubscribeEvent()
    public void onHarvest(BlockEvent.HarvestDropsEvent event) {
        IBlockState state = event.getState();
        if (state.isIn(BlockTags.LOGS)) {
            if (event.getHarvester() != null) {
                EntityPlayer player = event.getHarvester();
                if (PlayerUtils.isHolding(player, ConstantIngredients.AXE))
                    return; //Don't continue if player has an axe
            }

            event.getDrops().clear();
            ItemStack log = new ItemStack(state.getBlock());
            Relation relation = RelationaryManager.getRelationWithShape(log, Relationships.LOG);
            if (relation != null) {
                event.getDrops().add(relation.first(Relationships.BARK, BARK_COUNT.get()));
                event.getDrops().add(relation.first(Relationships.DUST, DUST_COUNT.get()));
                event.getDrops().add(relation.first(Relationships.PLANK, PLANK_COUNT.get()));

                if (SPECIAL_SOUND.get() && event.getWorld() instanceof World) {
                    EnvironmentUtils.playSound((World) event.getWorld(), event.getPos(), SoundEvents.ENTITY_ZOMBIE_BREAK_WOODEN_DOOR, SoundCategory.BLOCKS, 1f, 1f);
                }
            }
        }
    }

    @Override
    public String getDescription() {
        return "Logs that are broken with an axe shatter into planks, bark and dust.";
    }
}
