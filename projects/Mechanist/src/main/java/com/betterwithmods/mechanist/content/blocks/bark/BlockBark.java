/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.mechanist.content.blocks.bark;

import com.betterwithmods.core.Tags;
import com.betterwithmods.mechanist.Registrars;
import com.betterwithmods.mechanist.content.blocks.dynamic.mini.BlockSiding;
import com.betterwithmods.mechanist.content.tiles.TileCamoflage;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.Tag;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;

public class BlockBark extends BlockSiding {

    public static final EnumProperty<EnumFacing.Axis> AXIS = BlockStateProperties.AXIS;

    public BlockBark(Properties properties) {
        super(properties);
    }

    private static final VoxelShape[] SHAPES = new VoxelShape[]{
            Block.makeCuboidShape(0, 14, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 2, 16),
            Block.makeCuboidShape(0, 0, 14, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 2),
            Block.makeCuboidShape(14, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 2, 16, 16),
    };

    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        int i = state.get(FACING).getIndex();
        return SHAPES[i];
    }

    private static final Tag<Block> BARK_LOGS = Tags.Blocks.BARK_LOGS;

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        for (Block block : BARK_LOGS.getAllElements()) {
            ItemStack bark = TileCamoflage.fromParent(this, block.getDefaultState());
            items.add(bark);
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, IBlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(AXIS);
    }

    public static ItemStack fromParent(IBlockState state) {
        return fromParent(state, 1);
    }

    public static ItemStack fromParent(IBlockState state, int count) {
        Block block = state.getBlock();
        if (BARK_LOGS.contains(block)) {
            return TileCamoflage.fromParent(Registrars.Blocks.BARK, block.getDefaultState(), count);
        }
        return ItemStack.EMPTY;
    }

}
