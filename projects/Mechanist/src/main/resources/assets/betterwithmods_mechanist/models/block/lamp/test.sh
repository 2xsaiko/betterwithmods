COLORS=(white orange magenta light_blue yellow lime pink gray light_gray cyan purple blue brown green red black)



for color in "${COLORS[@]}"; do
    current="${color}_redstone_lamp.json"
    echo $color $current
    cp "./COLOR_redstone_lamp.json" "./$current"
    sed -i "s/COLOR/$color/g" "$current"
done
