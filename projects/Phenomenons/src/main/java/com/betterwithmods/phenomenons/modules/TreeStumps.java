/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Relationships;
import com.betterwithmods.core.base.game.client.baking.QuadReplacingModel;
import com.betterwithmods.core.base.setup.ModuleBase;
import com.betterwithmods.core.relationary.Relation;
import com.betterwithmods.core.relationary.RelationaryManager;
import com.betterwithmods.core.utilties.ModelUtils;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLog;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.BakedQuadRetextured;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.renderer.texture.MissingTextureSprite;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.IWorldReaderBase;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.data.IModelData;
import net.minecraftforge.client.model.data.ModelDataMap;
import net.minecraftforge.client.model.data.ModelProperty;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class TreeStumps extends ModuleBase {

    public static ForgeConfigSpec.ConfigValue<Float> STUMP_BASE_SPEED, ROOT_BASE_SPEED;
    public static ForgeConfigSpec.ConfigValue<Boolean> TOOLS_SCALE_BREAKING;

    public TreeStumps() {
        super("tree_stumps");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        STUMP_BASE_SPEED = builder.comment("Base speed for stump breaking, scaled by tools if option enabled")
                .translation("config.betterwithmods_phenomenons.stump_base_speed")
                .define("stump_base_speed", 0.03f);

        ROOT_BASE_SPEED = builder.comment("Base speed for root breaking, scaled by tools if option enabled")
                .translation("config.betterwithmods_phenomenons.root_base_speed")
                .define("root_base_speed", 0.01f);

        TOOLS_SCALE_BREAKING = builder.comment("If enabled, stumps and roots break speed will be sped up by tools")
                .translation("config.betterwithmods_phenomenons.tools_scale_breaking")
                .define("tools_scale_breaking", true);
    }

    @Override
    public void commonSetup(FMLCommonSetupEvent event) {
        addAll(
                Blocks.OAK_LOG,
                Blocks.SPRUCE_LOG,
                Blocks.BIRCH_LOG,
                Blocks.JUNGLE_LOG,
                Blocks.ACACIA_LOG,
                Blocks.DARK_OAK_LOG,
                Blocks.STRIPPED_OAK_LOG,
                Blocks.STRIPPED_SPRUCE_LOG,
                Blocks.STRIPPED_BIRCH_LOG,
                Blocks.STRIPPED_JUNGLE_LOG,
                Blocks.STRIPPED_ACACIA_LOG,
                Blocks.STRIPPED_DARK_OAK_LOG
        );
    }

    @Override
    public String getDescription() {
        return "Add Tree Stumps";
    }


    @SubscribeEvent
    public void blockBreakSpeed(PlayerEvent.BreakSpeed event) {
        World world = event.getEntityPlayer().getEntityWorld();
        BlockPos pos = event.getPos();
        if (isStump(world, pos)) {
            event.setNewSpeed(STUMP_BASE_SPEED.get() * ((TOOLS_SCALE_BREAKING.get()) ? event.getOriginalSpeed() : 1f));
        } else if (isRoot(world, pos)) {
            event.setNewSpeed(ROOT_BASE_SPEED.get() * ((TOOLS_SCALE_BREAKING.get()) ? event.getOriginalSpeed() : 1f));
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onHarvest(BlockEvent.HarvestDropsEvent event) {
        IWorld world = event.getWorld();
        BlockPos pos = event.getPos();
        IBlockState state = event.getState();
        if (isStump(state, world, pos)) {
            event.getDrops().clear();

            ItemStack log = new ItemStack(state.getBlock());
            Relation relation = RelationaryManager.getRelationWithShape(log, Relationships.LOG);
            if(relation != null) {
                event.getDrops().add(relation.first(Relationships.BARK, 1));
                event.getDrops().add(relation.first(Relationships.DUST, 1));
            }
        }
    }


    //TODO stump registration IMC, extract IMC to core?
    public static void addStump(Block block) {
        STUMPS.add(block.getRegistryName());
    }

    public static void addAll(Block... blocks) {
        for (Block block : blocks)
            addStump(block);
    }

    private static final Set<Material> ROOTABLE = Sets.newHashSet(Material.GROUND, Material.GRASS);

    public static boolean isStumpable(IBlockState state) {
        if (STUMPS.contains(state.getBlock().getRegistryName())) {
            if (state.has(BlockLog.AXIS)) {
                return state.get(BlockLog.AXIS).isVertical();
            }
        }
        return false;
    }

    public static boolean isRootable(IBlockState state) {
        Material material = state.getMaterial();
        return ROOTABLE.contains(material);
    }

    public static boolean isRoot(IWorldReaderBase world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        return isRoot(state, world, pos);
    }

    public static boolean isRoot(IBlockState state, IWorldReaderBase world, BlockPos pos) {
        IBlockState above = world.getBlockState(pos.up());
        return isRootable(state) && isStumpable(above);
    }

    public static boolean isStump(IWorldReaderBase world, BlockPos pos) {
        IBlockState state = world.getBlockState(pos);
        return isStump(state, world, pos);
    }

    public static boolean isStump(IBlockState state, IWorldReaderBase world, BlockPos pos) {
        IBlockState below = world.getBlockState(pos.down());
        return isStumpable(state) && isRootable(below);
    }

    private final static Set<ResourceLocation> STUMPS = Sets.newHashSet();

    @SubscribeEvent
    public void modelBake(ModelBakeEvent event) {
        Map<ModelResourceLocation, IBakedModel> registry = event.getModelRegistry();
        for (ModelResourceLocation location : registry.keySet()) {
            ResourceLocation block = new ResourceLocation(location.getNamespace(), location.getPath());
            if (STUMPS.contains(block)) {
                IBakedModel originalModel = registry.get(location);
                registry.put(location, new StumpModel(originalModel));
            }
        }
    }

    @SubscribeEvent
    public void textureStitch(TextureStitchEvent.Pre event) {
        TextureMap map = event.getMap();
        for (ResourceLocation block : STUMPS) {
            map.registerSprite(Minecraft.getInstance().getResourceManager(), new ResourceLocation(References.MODID_PHENOMENONS, String.format("block/stumps/%s", block.getPath())));
        }
    }

    @OnlyIn(Dist.CLIENT)
    public static class StumpModel extends QuadReplacingModel {
        private static final ModelProperty<Boolean> STUMP = new ModelProperty<>();

        private static final Cache<IBlockState, TextureAtlasSprite> SPRITE_CACHE = CacheBuilder.newBuilder().build();

        StumpModel(IBakedModel original) {
            super(original);
        }

        @Override
        public boolean shouldReplaceQuad(@Nonnull BakedQuad quad, @Nonnull IModelData data) {
            boolean isStump = data.hasProperty(STUMP) && data.getData(STUMP);
            return isStump && quad.getFace().getAxis().isHorizontal();
        }

        @Override
        public BakedQuad replaceQuad(@Nonnull BakedQuad quad, @Nonnull IModelData data, IBlockState state) {
            return new BakedQuadRetextured(quad, getSprite(state));
        }

        private TextureAtlasSprite getSprite(IBlockState state) {
            try {
                return SPRITE_CACHE.get(state, () -> findPrivate(state));
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return MissingTextureSprite.getSprite();
        }

        private TextureAtlasSprite findPrivate(IBlockState state) {
            String path = state.getBlock().getRegistryName().getPath();
            return ModelUtils.getSprite(new ResourceLocation(References.MODID_PHENOMENONS, String.format("block/stumps/%s", path)));
        }


        @Nonnull
        @Override
        public IModelData getModelData(@Nonnull IWorldReader world, @Nonnull BlockPos pos, @Nonnull IBlockState state, @Nonnull IModelData tileData) {
            ModelDataMap.Builder builder = new ModelDataMap.Builder();
            builder.withInitial(STUMP, isStump(world, pos));
            return builder.build();
        }
    }
}
