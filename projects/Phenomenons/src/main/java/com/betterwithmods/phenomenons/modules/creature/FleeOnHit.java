/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.phenomenons.modules.creature;

import com.betterwithmods.core.base.game.ai.AIFleeFromTarget;
import com.betterwithmods.core.base.setup.ModuleBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.List;

public class FleeOnHit extends ModuleBase {

    private ForgeConfigSpec.IntValue maximumFleeDistance;
    private ForgeConfigSpec.IntValue maximumFleeDuration;
    private ForgeConfigSpec.BooleanValue groupFlee;
    private ForgeConfigSpec.IntValue groupFleeRange;

    public FleeOnHit() {
        super("fleeOnHit");
    }

    @Override
    public void configs(ForgeConfigSpec.Builder builder) {
        getLoader().addCondition("fleeOnHit", this::isEnabled);
        this.maximumFleeDistance = builder.comment("The maximum distance an entity will flee from an attacker.")
                .translation("config.betterwithmods_phenomenons.maximumFleeDistance")
                .defineInRange("maxiumumFleeDistance", 10, 0, 44);

        this.maximumFleeDuration = builder.comment("The maximum number of ticks an entity will flee from an attacker.")
                .translation("config.betterwithmods_phenomenons.maximumFleeDuration")
                .defineInRange("maximumFleeDuration", 160, 0, Integer.MAX_VALUE);

        this.groupFlee = builder.comment("Should a fleeing entity cause other nearby entities to flee.")
                .translation("config.betterwithmods_phenomenons.groupFlee")
                .define("groupFlee", true);

        this.groupFleeRange = builder.comment("How large of a range should a fleeing entity cause other nearby entities to flee.")
                .translation("config.betterwithmods_phenomenons.groupFleeRange")
                .defineInRange("groupFleeRange", 10, 0, Integer.MAX_VALUE);
    }


    @Override
    public String getDescription() {
        return "When a passive mob is attacked it will attempt to flee from the attacker";
    }

    @SubscribeEvent
    public void addAI(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof EntityAnimal) {
            EntityAnimal entity = (EntityAnimal) event.getEntity();

            entity.targetTasks.addTask(0, new AIFleeFromTarget(entity, maximumFleeDistance.get(), maximumFleeDuration.get()));
        }
    }

    @SubscribeEvent
    public void onEntityAttacked(LivingDamageEvent event) {
        if(event.getEntity() instanceof EntityAnimal && event.getSource().getTrueSource() instanceof EntityLivingBase) {
            EntityAnimal entity = (EntityAnimal) event.getEntity();
            entity.setRevengeTarget((EntityLivingBase) event.getSource().getTrueSource());

            if(groupFlee.get()) {
                AxisAlignedBB aabb = new AxisAlignedBB(entity.getPosition());
                List<EntityLivingBase> entities = entity.getEntityWorld().getEntitiesWithinAABB(EntityAnimal.class, aabb.grow(groupFleeRange.get()));
                entities.forEach(entityLivingBase -> entityLivingBase.setRevengeTarget((EntityLivingBase) event.getSource().getTrueSource()));
            }
        }
    }
}
