/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.content.blocks;

import com.betterwithmods.vitality.content.tiles.TileBeacon;
import com.betterwithmods.vitality.modules.beacons.BeaconEffect;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

public class BlockBeacon extends net.minecraft.block.BlockBeacon {

    public BlockBeacon(Properties builder) {
        super(builder);
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return new TileBeacon();
    }

    @Override
    public boolean onBlockActivated(IBlockState state, World worldIn, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        TileEntity tileEntity = worldIn.getTileEntity(pos);

        if(tileEntity instanceof  TileBeacon) {
            TileBeacon tileBeacon = (TileBeacon) tileEntity;
            BeaconEffect effect = tileBeacon.getCurrentEffect();
            if(effect != null) {
                effect.onInteract(worldIn, pos, player, hand, player.getHeldItem(hand));
            }

            if(player.isCreative()) {
                ItemStack handStack = player.getHeldItem(hand);
                if(handStack.getItem() instanceof ItemBlock) {
                    Block block = ((ItemBlock) handStack.getItem()).getBlock();
                    IBlockState blockState = block.getDefaultState();
                    int r;
                    for (r = 1; r <= 4; r++) {
                        for (int x = -r; x <= r; x++) {
                            for (int z = -r; z <= r; z++) {
                                worldIn.setBlockState(pos.add(x, -r, z), blockState);
                            }
                        }
                    }

                }
            }
        }



        return super.onBlockActivated(state, worldIn, pos, player, hand, side, hitX, hitY, hitZ);
    }

    @Override
    public void onPlayerDestroy(IWorld worldIn, BlockPos pos, IBlockState state) {
        TileEntity tileEntity = worldIn.getTileEntity(pos);

        if(tileEntity instanceof  TileBeacon) {
            TileBeacon tileBeacon = (TileBeacon) tileEntity;
            BeaconEffect effect = tileBeacon.getCurrentEffect();
            if(effect != null) {
                effect.onBreak(worldIn.getWorld(), pos);
            }
        }
        super.onPlayerDestroy(worldIn, pos, state);
    }


}
