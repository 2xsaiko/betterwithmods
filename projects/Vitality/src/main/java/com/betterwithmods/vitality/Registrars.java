/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality;

import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.ConstantProperties;
import com.betterwithmods.core.base.game.item.FoodProperties;
import com.betterwithmods.core.base.game.item.ItemBase;
import com.betterwithmods.core.base.game.item.ItemFood;
import com.betterwithmods.core.base.game.item.ItemProperties;
import com.betterwithmods.core.base.registration.BlockRegistrar;
import com.betterwithmods.core.base.registration.CreativeTab;
import com.betterwithmods.core.base.registration.ItemRegistrar;
import com.betterwithmods.core.base.registration.TileEntityRegistrar;
import com.betterwithmods.vitality.content.blocks.BlockBeacon;
import com.betterwithmods.vitality.content.tiles.TileBeacon;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.init.MobEffects;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
public class Registrars {

    public static Registrars INSTANCE;

    private static String MODID;

    public static final CreativeTab ITEM_GROUP = new CreativeTab(References.MODID_VITALITY, "items", () -> new ItemStack(Items.WITCH_WART));

    public Registrars(String modid, Logger logger) {
        MODID = modid;
        INSTANCE = this;
        new BlockRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Block> registry) {
                //TODO - Override minecraft beacon. Currently gives error if you try.
                register(registry, "beacon", new BlockBeacon(ConstantProperties.ROCK));
            }
        };

        new TileEntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<TileEntityType<?>> registry) {
                register(registry, "beacon", Tiles.TILE_BEACON);
            }
        };
        new ItemRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Item> registry) {
                ItemProperties prop = ConstantProperties.MATERIALS.group(ITEM_GROUP);
                register(registry, "healing_gland", new ItemBase(prop));
                register(registry, "poison_sac", new ItemBase(prop));
                register(registry, "witch_wart", new ItemBase(prop));

                FoodProperties foodProp = new FoodProperties().group(ITEM_GROUP);
                FoodProperties foodPropRaw = foodProp.potionId(new PotionEffect(MobEffects.HUNGER, 600, 0)).potionEffectProbability(1);

                register(registry, "cocoa_powder", new ItemBase(prop.group(ITEM_GROUP)));
                register(registry, "donut", new ItemFood(foodProp.healAmount(2).saturationAmount(0.25f)));
                register(registry, "creeper_oysters", new ItemFood(foodProp.healAmount(2).saturationAmount(0.0f).potionId(new PotionEffect(MobEffects.POISON, 100, 0)).potionEffectProbability(1)));
                register(registry, "egg", new ItemFood(foodPropRaw.healAmount(2).saturationAmount(0.2f)));
                register(registry, "cooked_egg", new ItemFood(foodProp.healAmount(2).saturationAmount(0.25f)));
                register(registry, "scrambled_egg", new ItemFood(foodPropRaw.healAmount(4).saturationAmount(0.3f)));
                register(registry, "cooked_scrambled_egg", new ItemFood(foodProp.healAmount(5).saturationAmount(0.5f)));
                register(registry, "omelet", new ItemFood(foodPropRaw.healAmount(3).saturationAmount(0.5f)));
                register(registry, "cooked_omelet", new ItemFood(foodProp.healAmount(4).saturationAmount(1)));
                register(registry, "ham_and_eggs", new ItemFood(foodProp.healAmount(2).saturationAmount(0.25f)));
                register(registry, "tasty_sandwich", new ItemFood(foodProp.healAmount(7).saturationAmount(0.7f)));
                register(registry, "beef_dinner", new ItemFood(foodProp.healAmount(8).saturationAmount(0.6f)));
                register(registry, "beef_potatoes", new ItemFood(foodProp.healAmount(7).saturationAmount(0.5f)));
                register(registry, "chocolate", new ItemFood(foodProp.healAmount(2).saturationAmount(0.2f)));
                register(registry, "kebab", new ItemFood(foodPropRaw.healAmount(4).saturationAmount(0.3f)));
                register(registry, "cooked_kebab", new ItemFood(foodProp.healAmount(8).saturationAmount(0.4f)));
                register(registry, "pork_dinner", new ItemFood(foodProp.healAmount(8).saturationAmount(0.6f)));
                register(registry, "wolf_chop", new ItemFood(foodPropRaw.healAmount(3).saturationAmount(0.3f)));
                register(registry, "cooked_wolf_chop", new ItemFood(foodProp.healAmount(8).saturationAmount(0.3f)));
                register(registry, "kibble", new ItemFood(foodPropRaw.healAmount(3).saturationAmount(0.0f)));
                register(registry, "apple_pie", new ItemFood(foodProp.healAmount(8).saturationAmount(0.3f)));
                register(registry, "mystery_meat", new ItemFood(foodPropRaw.healAmount(2).saturationAmount(0.3f)));
                register(registry, "cooked_mystery_meat", new ItemFood(foodProp.healAmount(6).saturationAmount(0.8f)));
                register(registry, "bat_wing", new ItemFood(foodProp.healAmount(2).saturationAmount(0.3f)));
                register(registry, "cooked_bat_wing", new ItemFood(foodProp.healAmount(4).saturationAmount(0.6f)));

                //TODO - Soup
                //register(registry, "chowder", new ItemFood(foodProp.healAmount(5).saturationAmount(0.0f)));
                //register(registry, "hearty_stew", new ItemFood(foodProp.healAmount(10).saturationAmount(0.0f)));
                register(registry, "chicken_soup", new ItemFood(foodProp.healAmount(2).saturationAmount(0.25f)));

                register(registry, Blocks.BEACON, ITEM_GROUP);
            }
        };
    }

    @ObjectHolder(References.MODID_VITALITY)
    public static class Items {
        public static final Item WITCH_WART = null;
        public static final Item HEALING_GLAND = null;
        public static final Item POISON_SAC = null;
    }

    @ObjectHolder(References.MODID_VITALITY)
    public static class Blocks {
        public static final Block BEACON = null;
    }

    public static class Tiles {
        public static TileEntityType<TileBeacon> TILE_BEACON = TileEntityType.Builder.create(TileBeacon::new).build(null);
    }
}
