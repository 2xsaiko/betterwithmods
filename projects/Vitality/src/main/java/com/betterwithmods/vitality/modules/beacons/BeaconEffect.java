/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.modules.beacons;

import com.betterwithmods.core.References;
import com.betterwithmods.core.Tags;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tags.Tag;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.logging.log4j.util.TriConsumer;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.function.*;

public class BeaconEffect {

    private boolean enabled;
    private Tag<Block> structureTag;

    private int currentLevel;
    private EnumDyeColor beamColor;
    private Map<Integer, Integer> levelRanges;

    private Function<BeaconEffect, Predicate<EntityLivingBase>> applicationCriteria;
    private Map<Integer, List<PotionEffect>> potionEffects;
    private Function<BeaconEffect, BiConsumer<World, BlockPos>> onCreate;
    private Function<BeaconEffect, BiFunction<World, BlockPos, Consumer<EntityLivingBase>>> onApply;
    private Function<BeaconEffect, BiFunction<World, BlockPos, TriConsumer<EntityLivingBase, EnumHand, ItemStack>>> onInteract;
    private Function<BeaconEffect, BiConsumer<World, BlockPos>> onBreak;

    protected BiConsumer<ForgeConfigSpec.Builder, List<ForgeConfigSpec.ConfigValue>> addConfigs;


    public BeaconEffect(ResourceLocation tagLocation) {
        this.enabled = true;
        this.structureTag = Tags.Blocks.tag(tagLocation);
        this.beamColor = EnumDyeColor.WHITE;
        this.levelRanges = Maps.newHashMap();
        for(int level = 0; level <= 4; level++) {
            this.levelRanges.put(level, 20 * level);
        }
        this.applicationCriteria = beaconEffect -> entityLivingBase -> entityLivingBase instanceof EntityPlayer;
        this.potionEffects = Maps.newHashMap();
        this.onCreate = beaconEffect -> (world, blockPos) -> {};
        this.onApply = beaconEffect -> (world, blockPos) -> entityLivingBase -> {};
        this.onInteract = beaconEffect -> (world, blockPos) -> (entityLivingBase, enumHand, itemStack) -> {};
        this.onBreak = beaconEffect -> (world, blockPos) -> {};
        this.addConfigs = ((builder, configValues) -> {});
    }

    public BeaconEffect(String localTagName) {
        this(new ResourceLocation(References.MODID_VITALITY, "beacons/" + localTagName));
    }

    public void onCreate(World world, BlockPos blockPos) {
        this.onCreate.apply(this).accept(world, blockPos);
    }

    public void apply(World world, BlockPos blockPos) {
        AxisAlignedBB rangeBB = new AxisAlignedBB(blockPos).grow(levelRanges.get(currentLevel));
        List<EntityLivingBase> entitiesInRange = world.getEntitiesWithinAABB(EntityLivingBase.class, rangeBB);


        entitiesInRange.stream()
                .filter(entityLivingBase -> applicationCriteria.apply(this).test(entityLivingBase))
                .forEach(entityLivingBase -> {
                    potionEffects.getOrDefault(currentLevel, Lists.newArrayList())
                            .forEach(potionEffect -> {
                                entityLivingBase.addPotionEffect(new PotionEffect(potionEffect));
                            });
                    this.onApply.apply(this).apply(world, blockPos).accept(entityLivingBase);
                });
    }

    public void onInteract(World world, BlockPos blockPos, EntityPlayer player, EnumHand hand, ItemStack itemStack) {
        this.onInteract.apply(this).apply(world, blockPos).accept(player, hand, itemStack);
    }

    public void onBreak(World world, BlockPos blockPos) {
        this.onBreak.apply(this).accept(world, blockPos);
    }

    public BeaconEffect withRange(int level, int range) {
        this.levelRanges.put(level, range);
        return this;
    }

    public BeaconEffect withRange(Function<Integer, Integer> rangeFunc) {
        for(int level = 0; level < levelRanges.size(); level++) {
            this.levelRanges.put(level, rangeFunc.apply(level));
        }
        return this;
    }

    public BeaconEffect withPotionEffect(int level, PotionEffect potionEffect) {
        List<PotionEffect> effects = this.potionEffects.getOrDefault(level, Lists.newArrayList());
        effects.add(potionEffect);
        this.potionEffects.putIfAbsent(level, effects);
        return this;
    }

    public BeaconEffect withScalingPotionEffect(PotionEffect potionEffect) {
        for(int level = 1; level <= 4; level++) {
            this.withPotionEffect(level, new PotionEffect(potionEffect.getPotion(), potionEffect.getDuration(), potionEffect.getAmplifier() + level - 1));
        }
        return this;
    }

    public BeaconEffect withApplicationCriteria(Function<BeaconEffect, Predicate<EntityLivingBase>> applicationCriteria) {
        this.applicationCriteria = applicationCriteria;
        return this;
    }

    public BeaconEffect withBeamColor(EnumDyeColor beamColor) {
        this.beamColor = beamColor;
        return this;
    }

    public BeaconEffect setOnCreate(Function<BeaconEffect, BiConsumer<World, BlockPos>> createFunc) {
        this.onCreate = createFunc;
        return this;
    }

    public BeaconEffect setOnApply(Function<BeaconEffect,BiFunction<World, BlockPos, Consumer<EntityLivingBase>>> applyFunc) {
        this.onApply = applyFunc;
        return this;
    }

    public BeaconEffect setOnInteract(Function<BeaconEffect,BiFunction<World, BlockPos, TriConsumer<EntityLivingBase, EnumHand, ItemStack>>> interactFunc) {
        this.onInteract = interactFunc;
        return this;
    }

    public BeaconEffect setOnBreak(Function<BeaconEffect,BiConsumer<World, BlockPos>> breakFunc) {
        this.onBreak = breakFunc;
        return this;
    }

    public BeaconEffect setAddConfig(BiConsumer<ForgeConfigSpec.Builder, List<ForgeConfigSpec.ConfigValue>>  configFunc) {
        this.addConfigs = configFunc;
        return this;
    }


    public int getCurrentLevel() {
        return currentLevel;
    }

    public EnumDyeColor getBeamColor() {
        return beamColor;
    }

    public Tag<Block> getStructureTag() {
        return structureTag;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public ResourceLocation getTagResource() {
        return structureTag.getId();
    }

    public void applyConfigs(List<ForgeConfigSpec.ConfigValue> configs) {

    }
}
