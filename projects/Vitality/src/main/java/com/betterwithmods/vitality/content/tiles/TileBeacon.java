/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.vitality.content.tiles;

import com.betterwithmods.vitality.Registrars;
import com.betterwithmods.vitality.modules.beacons.BeaconEffect;
import com.betterwithmods.vitality.modules.beacons.EnhancedBeacons;
import com.google.common.collect.Lists;
import net.minecraft.block.Block;
import net.minecraft.block.BlockStainedGlass;
import net.minecraft.block.BlockStainedGlassPane;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBeacon;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class TileBeacon extends TileEntity implements ITickable {

    public final List<TileEntityBeacon.BeamSegment> beamSegments = Lists.newArrayList();
    @OnlyIn(Dist.CLIENT)
    private long beamRenderCounter;
    @OnlyIn(Dist.CLIENT)
    private float beamRenderScale;
    public boolean isComplete;
    public boolean lastTickComplete;

    public TileBeacon() {
        super(Registrars.Tiles.TILE_BEACON);
    }

    private BeaconEffect currentEffect;

    @Override
    public void tick() {
        if (this.world.getGameTime() % 80L == 0L) {
            this.updateBeacon();
            if (this.isComplete) {
                this.playSound(SoundEvents.BLOCK_BEACON_AMBIENT);
                currentEffect.apply(getWorld(), getPos());

            }
        }

        if (!this.world.isRemote && this.isComplete != this.lastTickComplete) {
            this.lastTickComplete = this.isComplete;
            this.playSound(this.isComplete ? SoundEvents.BLOCK_BEACON_ACTIVATE : SoundEvents.BLOCK_BEACON_DEACTIVATE);
            this.currentEffect.onCreate(getWorld(), getPos());
        }

    }

    public void updateBeacon() {
        determineEffect();
        if (currentEffect != null) {
            int levels = calculateLevels();
            this.isComplete = levels > 0;
            this.currentEffect.setCurrentLevel(levels);
            updateColorSegments();
        }
    }

    private void updateColorSegments() {
        int i = this.pos.getX();
        int j = this.pos.getY();
        int k = this.pos.getZ();
        TileEntityBeacon.BeamSegment beamSegment = new TileEntityBeacon.BeamSegment(currentEffect.getBeamColor().getColorComponentValues());
        this.beamSegments.add(beamSegment);
        boolean flag = true;
        BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();
        for(int k1 = j + 1; k1 < this.getWorld().getHeight(); ++k1) {
            IBlockState iblockstate = this.world.getBlockState(blockpos$mutableblockpos.setPos(i, k1, k));
            Block block = iblockstate.getBlock();
            float[] afloat;
            if (block instanceof BlockStainedGlass) {
                afloat = ((BlockStainedGlass)block).getColor().getColorComponentValues();
            } else if (!(block instanceof BlockStainedGlassPane)) {
                if (iblockstate.getOpacity(this.world, blockpos$mutableblockpos) >= 15 && block != Blocks.BEDROCK) {
                    this.isComplete = false;
                    this.beamSegments.clear();
                    break;
                }

                float[] custom = iblockstate.getBeaconColorMultiplier(this.world, blockpos$mutableblockpos, this.getPos());
                if (custom == null) {
                    beamSegment.incrementHeight();
                    continue;
                }

                afloat = custom;
            } else {
                afloat = ((BlockStainedGlassPane)block).getColor().getColorComponentValues();
            }

            if (!flag) {
                afloat = new float[]{(beamSegment.getColors()[0] + afloat[0]) / 2.0F, (beamSegment.getColors()[1] + afloat[1]) / 2.0F, (beamSegment.getColors()[2] + afloat[2]) / 2.0F};
            }

            if (Arrays.equals(afloat, beamSegment.getColors())) {
                beamSegment.incrementHeight();
            } else {
                beamSegment = new TileEntityBeacon.BeamSegment(afloat);
                this.beamSegments.add(beamSegment);
            }

            flag = false;
        }
    }

    public void playSound(SoundEvent p_205736_1_) {
        this.world.playSound((EntityPlayer)null, this.pos, p_205736_1_, SoundCategory.BLOCKS, 1.0F, 1.0F);
    }

    @OnlyIn(Dist.CLIENT)
    public List<TileEntityBeacon.BeamSegment> getBeamSegments() {
        return this.beamSegments;
    }

    @OnlyIn(Dist.CLIENT)
    public float shouldBeamRender() {
        if (!this.isComplete) {
            return 0.0F;
        } else {
            int i = (int)(this.world.getGameTime() - this.beamRenderCounter);
            this.beamRenderCounter = this.world.getGameTime();
            if (i > 1) {
                this.beamRenderScale -= (float)i / 40.0F;
                if (this.beamRenderScale < 0.0F) {
                    this.beamRenderScale = 0.0F;
                }
            }

            this.beamRenderScale += 0.025F;
            if (this.beamRenderScale > 1.0F) {
                this.beamRenderScale = 1.0F;
            }

            return this.beamRenderScale;
        }
    }

    private void determineEffect() {
        IBlockState stateBelow = this.world.getBlockState(getPos().down());
        EnhancedBeacons.beaconEffects.stream()
                .filter(beaconEffect -> stateBelow.isIn(beaconEffect.getStructureTag()))
                .findFirst()
                .ifPresent(beaconEffect -> this.currentEffect = beaconEffect);
    }

    private int calculateLevels() {
        final int MAX_LEVELS = 4;
        IBlockState stateAtPosition;
        int radius = 0;
        for (radius = 1; radius <= MAX_LEVELS; radius++) {
            for (int x = -radius; x <= radius; x++) {
                for (int z = -radius; z <= radius; z++) {
                    stateAtPosition = this.world.getBlockState(getPos().add(x, -radius, z));
                    if(!stateAtPosition.isIn(currentEffect.getStructureTag())) {
                        return radius - 1;
                    }
                }
            }
        }

        return radius - 1;
    }

    public BeaconEffect getCurrentEffect() {
        return currentEffect;
    }
}
