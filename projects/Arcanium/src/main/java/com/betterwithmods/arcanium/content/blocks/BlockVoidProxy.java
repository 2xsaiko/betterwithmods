/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium.content.blocks;

import com.betterwithmods.arcanium.content.tiles.TileVoidProxy;
import com.betterwithmods.core.base.game.block.BlockBase;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Particles;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockVoidProxy extends BlockBase {
    public BlockVoidProxy(Properties properties) {
        super(properties);
        setRenderLayer(BlockRenderLayer.CUTOUT);
        setIsFullCube(s -> false);
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(IBlockState state, IBlockReader world) {
        return new TileVoidProxy();
    }


    @Override
    public VoxelShape getShape(IBlockState state, IBlockReader worldIn, BlockPos pos) {
        return VoxelShapes.empty();
    }


    public static boolean canProxy(World world, BlockPos pos, IBlockState originalState) {
        //Ignore all tile entities, might come back to this later.
        if (world.getTileEntity(pos) == null) {
            if (!originalState.isAir(world, pos)) {
                //Ignore blocks with properties for now, basically only normal solids
                return true;
            }
        }
        return false;
    }

    public static boolean setProxyState(Block proxy, World world, BlockPos pos, int time) {
        IBlockState originalState = world.getBlockState(pos);
        if (canProxy(world, pos, originalState)) {
            world.setBlockState(pos, proxy.getDefaultState());
            TileEntity tile = world.getTileEntity(pos);
            if (tile instanceof TileVoidProxy) {
                TileVoidProxy voidTile = ((TileVoidProxy) tile);
                voidTile.setParentState(originalState);
                voidTile.setTime(time);
                EnvironmentUtils.forEachParticle(world, pos, Particles.PORTAL, 10, EnvironmentUtils.RANDOM_PARTICLE, EnvironmentUtils.SPEED);
                return true;
            }
        }
        return false;
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.INVISIBLE;
    }
}
