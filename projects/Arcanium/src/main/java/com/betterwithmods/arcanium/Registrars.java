/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium;

import com.betterwithmods.arcanium.content.blocks.BlockVoidProxy;
import com.betterwithmods.arcanium.content.items.ItemVoidHole;
import com.betterwithmods.arcanium.content.tiles.TileVoidProxy;
import com.betterwithmods.core.References;
import com.betterwithmods.core.base.game.item.ItemProperties;
import com.betterwithmods.core.base.registration.BlockRegistrar;
import com.betterwithmods.core.base.registration.CreativeTab;
import com.betterwithmods.core.base.registration.ItemRegistrar;
import com.betterwithmods.core.base.registration.TileEntityRegistrar;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber
public class Registrars {

    public static Registrars INSTANCE;

    private static String MODID;

    public static final CreativeTab ITEM_GROUP = new CreativeTab(References.MODID_ARCANIUM, "items", () -> new ItemStack(net.minecraft.init.Items.ENDER_EYE));

    public Registrars(String modid, Logger logger) {
        MODID = modid;
        INSTANCE = this;
        new BlockRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Block> registry) {
                register(registry, "void_proxy", new BlockVoidProxy(Block.Properties.create(Material.ROCK)));
            }
        };

        new TileEntityRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<TileEntityType<?>> registry) {
                register(registry, "void_proxy", Tiles.TILE_VOID_PROXY);
            }
        };
        new ItemRegistrar(modid, logger) {
            @Override
            public void registerAll(IForgeRegistry<Item> registry) {
                register(registry, "void_hole", new ItemVoidHole(new ItemProperties().group(ITEM_GROUP)));
            }
        };
    }

    @ObjectHolder(References.MODID_ARCANIUM)
    public static class Items {
        public static final Item VOID_HOLE = null;
    }

    @ObjectHolder(References.MODID_ARCANIUM)
    public static class Blocks {
        public static final Block VOID_PROXY = null;
    }

    public static class Tiles {
        public static TileEntityType<TileVoidProxy> TILE_VOID_PROXY = TileEntityType.Builder.create(TileVoidProxy::new).build(null);
    }
}
