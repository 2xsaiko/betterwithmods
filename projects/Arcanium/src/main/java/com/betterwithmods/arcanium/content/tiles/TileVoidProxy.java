/*
 * BetterWithMods
 *
 * Copyright (c) 2019 BetterWithMods
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.betterwithmods.arcanium.content.tiles;

import com.betterwithmods.arcanium.Registrars;
import com.betterwithmods.core.utilties.EnvironmentUtils;
import com.betterwithmods.core.utilties.FacingUtils;
import com.betterwithmods.mechanist.content.tiles.TileCamoflage;
import net.minecraft.block.state.BlockFaceShape;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.client.model.data.IModelData;

import javax.annotation.Nonnull;

public class TileVoidProxy extends TileCamoflage implements ITickable {

    private int time;

    public TileVoidProxy() {
        super(Registrars.Tiles.TILE_VOID_PROXY);
    }

    @Nonnull
    @Override
    public IModelData getModelData() {
        return EmptyModelData.INSTANCE;
    }

    @Override
    public void tick() {
        if(world.isRemote)
            return;
        if (getTime() <= 0) {
            revert(world, getParentState());
        }
        time--;
    }


    @Override
    public void read(NBTTagCompound compound) {
        super.read(compound);
    }

    @Override
    public NBTTagCompound write(NBTTagCompound compound) {
        return super.write(compound);
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void revert(@Nonnull World world, @Nonnull IBlockState revert) {
        world.setBlockState(pos, getParentState());
        world.removeTileEntity(pos);
        EnvironmentUtils.playSound(world, pos, SoundEvents.ENTITY_ENDERMAN_TELEPORT, SoundCategory.BLOCKS, 1, 0.75f);
    }

    @OnlyIn(Dist.CLIENT)
    public boolean shouldRenderFace(EnumFacing face) {
        IBlockState offsetState = world.getBlockState(pos.offset(face));
        if(offsetState.getBlock() == Registrars.Blocks.VOID_PROXY)
            return false;
        return FacingUtils.isFaceShapeInDirection(world,pos, face, shape -> shape != BlockFaceShape.UNDEFINED);
    }
}
